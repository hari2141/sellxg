<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="Sellxg is a platform where Manufacturer, Wholeseller, Retailer can buy and sell surplus fabric, home textile, salwar suit,saree at best price of whole textile industry.">
<meta name="keywords" content="Overstock Textiles, Surplus Fabric, Textile Stocks, Extrastock, Yarn Lots, Home Textile">
<meta name="og_url" property="og:url" content="http://www.sellxg.com"></meta>
<meta name="author" content="SellXG">
<link href="inc/css/comingsoon.css" rel="stylesheet" type="text/css" />
<style>
	.coleft{
		float:right;
	}
	.coright{
		float:right;
	}
</style>
<?php
	require_once('tophead.php');
 ?>
<body>
<header class="nav-slider">
<?php

	require_once('menu.php');
 ?>
  <div class="row">
      <div class="col-md-12">
        <div class="page-location" ><strong></strong><a href="http://www.sellxg.com/index.php">Home</a><span>>></span><a href="http://www.sellxg.com/aboutus.php">About Us</a></div>
        <div class="mr-1000 mr-1000-location"></div>
      </div>
    </div>
</header>
<div class="row mainaccountdiv">
		<div class="col-md-push-2 col-md-10 col-sm-12 col-xs-12">
			<div class="col-md-9 myaccountinfo">
				<div class="col-sm-12 col-md-12 col-xs-12" style="border:1px solid #E3AE1E;">
					<div class="row" style="padding:9px;background:#E3AE1E;text-align:left;font-size:15px;color:#fff;">
						<font>
							 
						</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:15px;">
					<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px 0px;border-bottom:1px solid #eee;">	
						<h4>About Us</h4>
					</div>
					<div class="col-md-12 col-xs-12 col-sm-12 " style="margin-top:20px;">
					<div class="col-md-12" style="text-align:left;">
				<p style="text-align:justify;">	
            	   <i class="fa fa-circle-thin" aria-hidden="true"></i>Sellxg is Buyer to Buyer Supportive Online Portal. We created a platform for Selling/Buying of Wholesale,Available and Surplus goods.<br>
                  <i class="fa fa-circle-thin" aria-hidden="true"></i>We follow Authentic Approach in Buying and Selling Surplus, Available goods.<br>
				  <i class="fa fa-circle-thin" aria-hidden="true"></i>The main motto of Sell Xg is to bridge the gap between Buyer and Seller by creating a Customer oriented floor.<br>
				  <i class="fa fa-circle-thin" aria-hidden="true"></i>Buyer can Easily Buy the products which are placed on the website which is also a Time Saving Process.<br>
				  <i class="fa fa-circle-thin" aria-hidden="true"></i>We have dispute committee to manage any issues which are Arised During buying the Products.<br>
				  <i class="fa fa-circle-thin" aria-hidden="true"></i>Our Positives are Easy and Quick Transaction,Wide range of Items and Secured,Verified Payment Gateway for Buyers.<br><br></p>
                 <h4><i class="fa fa-circle" aria-hidden="true" ></i>Unique Feature : </h4><br />
				 <div class="padleft">
                 <ul>
                	 <li>1. Targeting to niche segment of Outdated Stocks.</li>
                     <li>2. Direct Sale to get optimum price of goods as per requirement.</li>
                     <li>3. Escrow system to enable payment security.</li>
                     <li>4. Online Dispute committee and 24/7 customer support to ensure smooth operations.</li>
                     <li>5. Transparent return policy to make it more reliable system</li>
                 </ul>
				 </div><br><br>
                </div>
				
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
		
	</div>
<div class="row">
	
</div>

<!--<div class="lazy-load">
  <div class='Cube panelLoad'>
    <div class='cube-face cube-face-front'>S</div>
    <div class='cube-face cube-face-back'>E</div>
    <div class='cube-face cube-face-left'>L</div>
    <div class='cube-face cube-face-right'>L</div>
    <div class='cube-face cube-face-bottom'>X</div>
    <div class='cube-face cube-face-top'>G</div>
  </div>
</div>-->
<!----footer ----->
<?php
	require_once('footer.php');
?>
</body>

<!--- Vijay add JS all is working Site ---->

<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/scrollReveal.min.js'></script>
<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/js'></script>
<!--<script type='text/javascript' src='http://www.themecop.com/wp/trendz/wp-content/themes/trendz/js/jquery.animateSlider.min.js?ver=4.5.2'></script>-->
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/core.min.js'></script>
<script type='text/javascript' src='js/widget.min.js'></script>
<script type='text/javascript' src='js/mouse.min.js'></script>
<script type='text/javascript' src='js/draggable.min.js?'></script>
<script type='text/javascript' src='js/slider.min.js'></script>
<script type='text/javascript' src='js/iris.min.js?'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var trendz_settings = {"latitude":"-37.8173306","longitude":"144.9556518","map_address":"on","map_title":"Envato","loading_time":"40000"};
/* ]]> */
</script>
<script type='text/javascript' src='js/main.js'></script>
</html>
