<?php 
	require_once('db.php');
	require_once('head.php');
	$_SESSION['wishlist']="index.php";
	
	if(isset($_POST['searchbtn']))
	{
		$c=$_POST['category'];
		$s=$_POST['search'];
		echo "<script>location.href='search.php?s=$s&c=$c';</script>";
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type"  content="text/html; charset=utf-8">
	<meta name="format-detection" content="telephone=no">
	<META NAME="Description" CONTENT="Sellxg is a platform where Manufacturer, Wholeseller, Retailer can buy and sell surplus fabric, home textile, salwar suit,saree at best price of whole textile industry." />
	<META NAME="Keywords" CONTENT="sellxg Surplus Fabric,sellxg Textile Stocks,sellxg Extrastock,sellxg Yarn Lots" />
	<meta name="robots" content="index,follow">
	<!-- Open Meta Data -->
	<meta property="og:title" content="SellXG " /><meta property="og:site_name" content="sellxg"/><meta property="og:url" content="http://www.sellxg.com/" /><meta property="og:image" content="http://www.sellxg.com/images/logo.png" /><meta property="og:image:url" content="http://www.sellxg.com/images/logo.png" /><meta property="og:image:width" content="250" /><meta property="og:image:height" content="250" /><meta property="og:description" content="Sellxg is a platform where Manufacturer, Wholeseller, Retailer can buy and sell surplus fabric, home textile, salwar suit,saree at best price of whole textile industry." /> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="canonical" href="http://www.sellxg.com"/>
</head>

	  
<!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="Sellxg is a platform where Manufacturer, Wholeseller, Retailer can buy and sell surplus fabric, home textile, salwar suit,saree at best price of whole textile industry.">
<meta name="keywords" content="Overstock Textiles, Surplus Fabric, Textile Stocks, Extrastock, Yarn Lots, Home Textile">
<meta name="og_url" property="og:url" content="http://www.sellxg.com"></meta>
<meta name="author" content="SellXG">-->

<title>Sell XG</title>
</head>
 

<body>
<!---- Top Header code start --->
<?php 
	require_once('tophead.php');
?>

<!---- Top Header code End ---> 

<!---- Top Header code start --->

<header class="nav-slider">
<?php  
	require_once('menu.php');
 ?>  
    <div class="row">
      <div class="col-md-12">
        <div class="main-slider mr-1000-top">
          <ul class="anim-slider">
            <li class="anim-slide"> <img src="images/banner_img2.jpg" id="image-one" alt="" >
              <h2 id="title-one">Sarees</h2>
              <p id="text-one">Best quality Sarees with Exotic Designs <br />
                and we are providing those which will Exceed <br />
                the Customer Expectation </p>
              <a href="http://www.sellxg.com/ladies/Sarees/?cat=Sarees" id="btn-one" class="">VIEW ALL ITEMS</a> </li>
            <li class="anim-slide"> <img src="images/banner_img1.jpg" id="image-two" alt="" >
              <h2 id="title-two">Kurtis</h2>
              <p id="text-two">I dress myself, not to impress, but for comfort and for style.<br>
                Find Best of Quality and Vibrant Kurtis Here</p>
              <a href="http://www.sellxg.com/ladies/Kurtis/?cat=Kurtis" id="btn-two" class="">VIEW ALL ITEMS</a> </li>
            <li class="anim-slide"> <img src="images/banner_img4.jpg" id="image-three" alt="">
              <h2 id="title-three">Salwar kameez</h2>
              <p id="text-three">Exclusive range of superior quality,<br> Attractive Look are Positives of Our Kurtis<br>So what are you waiting for ?
                </p>
              <a href="http://www.sellxg.com/ladies/Salwar%20Kameez/?cat=Salwar%20Kameez" id="btn-three" class="">VIEW ALL ITEMS</a> </li>
            <li class="anim-slide"> <img src="images/banner_img5.jpg" id="image-four" alt="" >
              <h2 id="title-four">Fabric</h2>
              <p id="text-four">With even Finishing & Grandeur looks <br> we suits the demands of customers <br />
               looking for Exclusive Products in the Eabric
              </p>
              <a href="http://www.sellxg.com/ladies/Fabrics/?cat=Fabrics" id="btn-four" class="">VIEW ALL ITEMS</a> </li>
          </ul>
          <div class="mr-1000 mr-1000-left"></div>
        </div>
      </div>
    </div>
  </div>
</header>

<!---- Header code End --->

<section>
  <div class="main_search_box">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="searchbox_home">
             <div class="left_title_box">
             <label>Search by <br />category</label>
             <form method="POST"  action="index.php" id="search1">
                <input type="text" placeholder="Enter search terms..." id="search-terms1" name="search" class="search_textboxhome" required>
                <select name="category" class="search_textboxselect">
					<option value="">All</option>
                 <?php 
				 $cat=mysql_query("select * from ".CATEGORY ." ");
				 while($category=mysql_fetch_array($cat))
				 {
					 echo "<option>$category[category]</option>"; 
				 }
				 ?>
                 
                </select>

                 <input type="submit" value="Search" name='searchbtn' class="button_searchbox" />
                 

              </form>
             </div>
          </div>
          <!---div class="search_box_home">
          
            <div class="search_skewx_zeero">
              <form method="POST" action="search.php" id="search">
                <input type="text" placeholder="Enter search terms..." id="search-terms" name="search">
                <i id="search-icon" class="fa fa-search"></i>
              </form>
            </div>
          </div--->
        </div>
      </div>
    </div>
  </div>
  <div class="featured_product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="mail_product_box">
            <div class="title_divproduct">
              <h2 >FEATURED PRODUCTS</h2>
              <a href="featuredproduct.php">VIEW ALL <i class="fa fa-angle-right fa-6" style="color:#fff;" aria-hidden="true"></i></a> </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="item_boxproduct">
              <ul>
			  <?php
			  $resultfeatured=mysql_query("select * from sf_product where featuredProduct=1 order by created_at DESC limit 0,3");
			  while($featured=mysql_fetch_array($resultfeatured))
			  {
				$selpro=mysql_query("select * from ".PRODUCT1." where serverProductId='".$featured[1]."' and status=1 ");
				while($fetpro=mysql_fetch_array($selpro))
				{
					$selimg=mysql_query("select * from ".IMAGE ." where serverProductId=$fetpro[0] limit 0,1");
					$fetimg=mysql_fetch_array($selimg);
			  ?>
					<li class="col-md-4 col-sm-6">
                  <div class="produt_innerbox">
                    <div class="product_imgbox">
                      <div class="center_img_box"> <a href="<? echo $fetpro['url']; ?>?pid=<?php echo $fetpro[0]; ?>"><img src="sellxg/admin/upload/<?php echo $fetimg[3]; ?>" /></a> </div> 
                    </div>
                    <div class="product_textdetails">
                      <div class="left_con_text">
                        <h3><a href="<? echo $fetpro['url']; ?>?pid=<?php echo $fetpro[0]; ?>"><?php echo $fetpro['productName']; ?></a></h3>
                        <p><a href="<? echo $fetpro['url']; ?>?pid=<?php echo $fetpro[0]; ?>">
						<?php 	
							$str1=substr($fetpro['description'],0,30);
							echo $str1."<br>";
							$str2=substr($fetpro['description'],30,20);
							echo $str2;
						?>
						</a></p>
                      </div>
                      <div class="right_con_text">
                        <div class="right_con_text1">
							
                          <span>
						  <?php 
						  $qty=explode("/",$fetpro['quantity']);
							if(!isset($_SESSION['currency']))
							{
								echo "
						<i class='fa  fa-rupee' aria-hidden='true'></i>"."".$fetpro['rate']."" ;
							echo " /".$qty[1];	
							}
							else   
							{
								echo "$ ". round($fetpro['rate'] / $_SESSION['currency'],2);
								echo " /".$qty[1];
							}
							?>
							
						  <i class="fa fa-shopping-bag fa-6" aria-hidden="true"></i>
						  </span> </div>
						 <!--- <div class="right_con_text1">
						  <span>
                           <h3><a href="checkout.php?pid=<?php echo $fetpro[0]; ?>" style="color: #ffffff;"><i class="fa" title='Buy Now' aria-hidden="true" >Buy Now</i></a></h3></span>
                      </div>--->
                    </div>
                  </div>
                </li>
			  <?php
				}
			  }
			  ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="letest_product_box">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title_divvj">
            <div class="center_box_title"> <samp class="line_border">
              <h3>BEST DEAL</h3>
              </samp> </div>
            <h2>LATEST PRODUCTS</h2>
          </div>
        </div>
        <div class="col-md-12">
          <div data-ride="carousel" class="carousel content-top-item-carousel ads-item-carousel-area slide">
            <div class="carousel-inner">
              <div class="item active">
                 <div class="row">
				 <?php
				 $resultlatest=mysql_query("select * from ".SF_PRODUCT ." where latestProduct=1 order by created_at DESC limit 0,4");
				while($latest=mysql_fetch_array($resultlatest))
				{ 
					$sellatpro=mysql_query("select * from ".PRODUCT1 ." where serverProductId='".$latest[1]."' and status=1 ");
					while($fetlatpro=mysql_fetch_array($sellatpro))
					{
						$selimg=mysql_query("select * from ".IMAGE ." where serverProductId=$fetlatpro[0] limit 0,1");
						$fetimg=mysql_fetch_array($selimg);
					?>
		
                    <div class="col-md-3">
                      <div class="product_latestdiv">
                        <div class="product_imgdiv">
                           <div class="product_imgbox">
                              <div class="center_img_box"> <a href="<? echo $fetlatpro['url']; ?>?pid=<?php echo $fetlatpro[0]; ?>"><img src="sellxg/admin/upload/<?php echo $fetimg[3]; ?>"></a> </div>
                           </div>
                       </div>
                        <div class="content_div_lestestpro">
						
                          <div class="product_button">
                             <div class="product_button_slwl">
                                <div class="product_button_skew">
                                  <a href="<? echo $fetlatpro['url']; ?>?pid=<?php echo $fetlatpro[0]; ?>"><?php echo $fetlatpro['productName']; ?></a>
                               </div>
                             </div>
                            
                          </div>
                          <p><a href="<? echo $fetlatpro['url']; ?>?pid=<?php echo $fetlatpro[0]; ?>" style="color:black;"> 
						  <?php 
								$str=substr($fetlatpro['description'],0,50);
								echo $str."..";
						   ?>
						  </a></p>
						  </div>
                          <div class="product_pricediv">
						  <div class="latest">
                            <span>
							<?php 
							$qty=explode("/",$fetlatpro['quantity']);
							//$price=explode("/","$fetlatpro['rate']");
							if(!isset($_SESSION['currency']))
							{
								echo "<i class='falatest fa-rupee' aria-hidden='true'></i>"."".$fetlatpro['TotallotRate']."";
								echo " /".$qty[1]."";
							}
							else   
							{
								
								echo "$ ". round($fetlatpro['TotallotRate'] / $_SESSION['currency'],2);
								echo " /".$qty[1];
							} 
							//echo "/".$price[1];
							?></span>
                          </div>
                          </div>
						  
                          <div class="product_cardbox">
						  
						  <?php if(!isset($_SESSION['userid'])){ ?>
							  <div class="card_boxicon"><a href="add_wishlist.php?pid=<?php echo $fetlatpro[0]; ?>"><i  class="fa fa-heart-o" aria-hidden="true" title='Add Wishlist'></i></a></div>
						<?php  }else { 
							$wlist = mysql_query("select * from ".WISHLIST ." where serverProductId='".$fetlatpro[0]."' and user_id='".$_SESSION['userid']."' ");
							$wresult=mysql_fetch_array($wlist);
							if(mysql_num_rows($wlist)>0){
							?>
							  <div class="card_boxicon"><a onclick='return confirm("Are you sure Remove item for wishlist");' href="delete_wishlist.php?wid=<?php echo $wresult['serverProductId']; ?>"  ><i  class="fa fa-heart" aria-hidden="true" title='Added Wishlist'></i></a></div>
							
							<?php  }else{ ?>
								<div class="card_boxicon"><a href="add_wishlist.php?pid=<?php echo $fetlatpro['serverProductId']; ?>"><i  class="fa fa-heart-o" aria-hidden="true" title='Add Wishlist'></i></a></div>
							<?php }}?>
                            
							
							<div class="button_colorpadding" ><div class="index"><a href="<? echo $fetlatpro['url']; ?>?pid=<?php echo $fetlatpro['serverProductId']; ?>"  class="margin-left btn btn-primary btn-lg ">View Details</a></div></div>
						
                          </div>
                      </div>
                      
                    </div>
                <?php
				}
				}
				?>				
                 </div>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="shopping_add shopping_add_bgimg ">
   <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <div class="shopping_add_out">
             <div class="shopping_add_inner">
              <span><img src="images/user.png" /></span>
              <div class="left">
              <h3>verified users</h3>
              <p>Verified Buyers and Users All <br> over the World</p>
              </div>
             </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="shopping_add_out">
             <div class="shopping_add_inner">
              <span><img src="images/monyimg.jpg" /></span>
              <div class="left">
              <h3>product quality</h3>
              <p>100% Product Quality Assourance  </p>
              </div>
             </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="shopping_add_out">
             <div class="shopping_add_inner">
              <span><img src="images/24call.jpg" /></span>
              <div class="left">
              <h3>24 HOURS SUPPORT</h3>
              <p>Call : +91 704-6075-057</p>
              </div>
             </div>
          </div>
        </div>
      </div>
   </div>
  </div>
  
  <div class="letest_product_box">
    <div class="container">
       <div class="row">
         <div class="col-md-12">
          <div class="title_divvj">
            <div class="center_box_title"> <samp class="line_border">
              <h3>GREAT DEAL</h3>
              </samp> </div>
            <h2>HIGHLY VIEWED PRODUCTS</h2>
          </div>
        </div>
        <div class="col-md-12">
          <div class="product_list">
         <div class="inner">
               <ul>
			   <?php
			   $resulthigh=mysql_query("SELECT *, COUNT('serverProductId') as pcount FROM `".VIEWED ."` group by `serverProductId` order by pcount desc limit 0,3");
			   while($high=mysql_fetch_array($resulthigh))
			   {
				   $productid=$high['serverProductId'];
				   $productresult=mysql_query("select * from ".PRODUCT1 ." where serverProductId='".$productid."' and status=1 ");
				   while($product=mysql_fetch_array($productresult))
				   {
			   ?>
               <li class="col-md-4 col-sm-6" >
               <div class="item_div">
                
                   <div class="images_box">
                      <div class="center_img_box">
                       <a href="<? echo $product['url']; ?>?pid=<?php echo $product[0]; ?>"><img src="sellxg/admin/upload/<?php 
					    $selimg=mysql_query("select * from ".IMAGE ." where serverProductId=$product[0] limit 0,1");
						$fetimg=mysql_fetch_array($selimg);
						echo $fetimg[3];?>"> </a>
                     </div>
                   </div>
                   <div class="title_pricediv" >
                   <div class="title_producename"><a href="<? echo $product['url']; ?>?pid=<?php echo $product[0]; ?>">
                     <?php echo $product['productName']; ?>
					 </a>
                   </div>
                    <div class="title_price">
					<div class="item" >
                      <a href="<? echo $product['url']; ?>?pid=<?php echo $product[0]; ?>">
                      <?php 
							$qty=explode("/",$product['quantity']);
							if(!isset($_SESSION['currency']))
							{
								echo "<i class='fa fa-rupee' aria-hidden='true'></i>"."" .$product['TotallotRate']."";
								echo " /".$qty[1];
							}
							else   
							{

								echo "$ ". round($product['TotallotRate'] / $_SESSION['currency'],2);
								echo " /".$qty[1];

							}
							?>
                            </a>
                   </div>
                   </div>
                   
                   </div>
				   <!--a href="product details.php?pid=<?php echo $product[0]; ?>">
                   <div class="button_droduct_list button_droduct_height">
                     <div class="new_special_textbox" style="width: 345px;">
                       <p><?php $str=substr($product['description'],0,80);
								echo $str . ".."; ?></p>
                     </div>
                   </div></a--->
              </div>
           </li>
				   <?php } 
			   }?>
            </ul>
          </div>
          </div>
        </div>
       </div>
     </div>  
  </div>
 
  <div class="blog_divhome">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="title_divvj">
            <div class="center_box_title"> <samp class="line_border">
              <h3>BEST BLOG</h3>
              </samp> </div>
            <h2>LATEST BLOG</h2>
			<h4 style="float:right;"><a href="blog.php" style="color:#e3ae1e;"><b>View All <i class="fa fa-angle-right fa-6" aria-hidden="true"></i></b></a></h4>
          </div>
          
        </div>
        
        <div class="col-md-12">
          <div class="carousel content-top-item-carousel ads-item-carousel-area slide" data-ride="carousel">
            <div class="carousel-inner">
			<?php
				$selblog=mysql_query("select * from blog order by blog_id DESC");
				$i=0;
				while($fetblog=mysql_fetch_array($selblog))
				{
					$i++;
					if($i==1)
					{
			?>
						<div class="item active">
                 <a href="<?php if(!isset($_SESSION['userid'])){
						echo "login.php";
				 }else{
					 echo "showblog.php?bid=$fetblog[0]";
				 }?>">
				 <div class="main_blogbox">
                   <div class="left_blog_part">
                     <div class="inner_blogbg">
                       <h3><?php echo $fetblog[1] ?></h3>
                       <span><strong>Posted By    :  </strong><?php echo $fetblog[2]; ?></span>
                       <span><strong>Posted On  :  </strong><?php echo $fetblog[6] ?></span>
                       <p>
					   <?php
							$s=substr($fetblog[5],0,200);
							echo $s;
						?>
						.</p>
                     </div>
                   </div>
                   <div class="right_blog_par">
                     <img src="images/<?php echo $fetblog[4] ?>" />
                   </div>
                 </div>
				 </a>
              </div>
			<?php
					}
					else
					{
			?>
						<div class="item">
						<a href="showblog.php?bid=<?php echo $fetblog[0]; ?>">
                 <div class="main_blogbox">
                   <div class="left_blog_part">
                     <div class="inner_blogbg">
                       <h3><?php echo $fetblog[1] ?></h3>
                       <span><strong>Posted By    :  </strong><?php echo $fetblog[2]; ?></span>
                       <span><strong>Posted On  :  </strong><?php echo $fetblog[6] ?></span>
                       <p><?php $s=substr($fetblog[5],0,200);
							echo $s; ?>.</p>
                     </div>
                   </div>
                   <div class="right_blog_par">
                     <img src="images/<?php echo $fetblog[4] ?>" style="img img-responsive"/>
                   </div>
                 </div>
				 </a>
              </div>
			<?php
					}
			?>
				
				
			  <?php
				}
			?>
             
            </div>
          </div>
        </div>
        
      </div>
    </div>  
  </div>
  
  <div class="clients_testimonials">
    <div class="container">
      <div class="row">
         <div class="col-md-12">
            <div id="quote-carousel" data-ride="carousel" class="carousel slide back_color_overflow">
                 <!-- Bottom Carousel Indicators -->
                 <div class="back_color_testi">
                 <h4>Brand We Have Worked With</h4>
                 <!---<ol class="carousel-indicators">
                 <li class="active" data-slide-to="0" data-target="#quote-carousel"><img alt="" src="images/testimonials-img1.jpg" class="img-responsive "> </li>
                 <li class="" data-slide-to="1" data-target="#quote-carousel"><img alt="" src="images/testimonials-img2.jpg" class="img-responsive "> </li>
                 <li class="" data-slide-to="2" data-target="#quote-carousel"><img alt="" src="images/testimonials-big1.jpg" class="img-responsive "> </li>
                 </ol>--->
                 </div>
                 <!-- Carousel Slides / Quotes -->
                   <div class="carousel-inner text-center">
                   <!-- Quote 1 -->
                   <div class="item active">
                        <blockquote>
                           <div class="row">
                             <div class="col-sm-12">
								<p class="arousel-testimonials">
								<div class="carousel-testimonials">
									<img src="images/GardenVareli.jpg" 
									class="img-width" />
									<img src="images/donear.png" class="img-donear"  />
									<img src="images/ddecor.png" class="img-decor" />
												</div>
										 </p>
                                                
                                               
                                            </div>
                                        </div>
                                        <!--<div class="row">
                                          <div class="testmo_name_border">
                                            <div class="inner_bottom_line">
                                                   </div>
                                                </div>
                                        </div>-->
                                    </blockquote>
                                </div>
                        </div>
         </div>
      </div>
     </div> 
  </div>
</section>


<!----footer ----->
<?php 
	require_once('footer.php');
?>
</body>

<!---

<div class="lazy-load">
  <div class='Cube panelLoad'>
    <div class='cube-face cube-face-front'>S</div>
    <div class='cube-face cube-face-back'>E</div>
    <div class='cube-face cube-face-left'>L</div>
    <div class='cube-face cube-face-right'>L</div>
    <div class='cube-face cube-face-bottom'>X</div>
    <div class='cube-face cube-face-top'>G</div>
  </div>
</div>
</body>

<!--- Vijay add JS all is working Site ---->

<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/scrollReveal.min.js'></script>
<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/js'></script>
<script type='text/javascript' src='js/jquery.animateSlider.min.js'></script>
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/core.min.js'></script>
<script type='text/javascript' src='js/widget.min.js'></script>
<script type='text/javascript' src='js/mouse.min.js'></script>
<script type='text/javascript' src='js/draggable.min.js?'></script>
<script type='text/javascript' src='js/slider.min.js'></script>
<script type='text/javascript' src='js/iris.min.js?'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var settings = {"latitude":"-37.8173306","longitude":"144.9556518","map_address":"on","map_title":"Envato","loading_time":"40000"};
/* ]]> */
</script>
<script type='text/javascript' src='js/main.js'></script>
</html>