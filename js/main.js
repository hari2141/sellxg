jQuery(document).ready(function($) {

    'use strict';

    $('.trendz-wc-product-slider-full .owl-carousel').owlCarousel({
        nav: true,
        dots: false,
        margin: 30,
        navText: [
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-left'></i></div>",
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-right'></i></div>"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                itmes: 2
            },
            767: {
                items: 3
            },
            991: {
                items: 4
            },
            1199: {
                items: 6
            }
        }
    });

    $('.trendz-wc-product-slider-full-col-4 .owl-carousel').owlCarousel({
        nav: true,
        dots: false,
        margin: 30,
        navText: [
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-left'></i></div>",
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-right'></i></div>"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                itmes: 2
            },
            767: {
                items: 3
            },
            1199: {
                items: 4
            }
        }
    });

    $('.trendz-wc-product-slider-half .owl-carousel').owlCarousel({
        nav: true,
        dots: false,
        margin: 30,
        navText: [
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-left'></i></div>",
            "<div class='icon-wrap-nav'><i class='tz tz-arrow-right'></i></div>"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            500: {
                itmes: 2
            },
            767: {
                items: 3
            }
        }
    });

    $('.feature-brands .owl-carousel').owlCarousel({
        nav: false,
        dots: false,
        margin: 30,
        navText: ['&#xf104;', '&#xf105;'],
        autoplay: true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        smartSpeed: 450,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            767: {
                items: 3
            },
            991: {
                items: 4
            }
        }
    });

    (function() {
        $(document).on('scroll', function() {
            if ($(window).width() > 500) {
                if ($('.tc-footer').isOnScreen()) {
                    $('#goto').fadeIn('slow');
                } else {
                    $('#goto').fadeOut('slow');
                }
            }
        });
        $('#goto').click(function(event) {
            event.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 800, 'linear');
        });
    })();

    $.fn.isOnScreen = function() {
        var viewport = {};
        viewport.top = $(window).scrollTop();
        viewport.bottom = viewport.top + $(window).height();
        var bounds = {};
        bounds.top = this.offset().top;
        bounds.bottom = bounds.top + this.outerHeight();
        return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
    };

    $('#accordion').on('show.bs.collapse', function(e) {
        $('#accordion .panel-heading.active').removeClass('active');
        $(e.target).prev().addClass('active');
    });

    window.sr = new scrollReveal();

    $(".qty-minus").click(function() {
        var inp = $(this).next();
        if (inp.val() > 1) {
            inp.val(inp.val() - 1);
        }
    });

    $(".qty-plus").click(function() {
        var inp = $(this).prev();
        var val = inp.val();
        inp.val(++val);
    });

    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }

    $(window).on('resize', function() {
        $('.trendz-subscription:visible').each(reposition);
    });

    (function() {
        if ($('.trendz-subscription').find('.mc4wp-alert').length != 0) {
            setTimeout(function() {
                $('.trendz-subscription').modal('show');
            }, 500);
        } else {
            setTimeout(function() {
                $('.trendz-subscription').modal('show');
            }, trendz_settings.loading_time);
        }
        $('.trendz-subscription').on('show.bs.modal', reposition);
    })();

    $('.trendz-wc-cat-down').click(function() {
        $(this).parent().toggleClass('active');
        $(this).next().slideToggle();
    });

    $(document).on("click", ".show-map", function(e) {
        e.preventDefault();
        $(".map-container").toggleClass("active")
    });

    (function() {
        var windowWidth = $('body').width();
        var containerWidth = $(".container").width();
        var width = $('#map').width();
        var left = (windowWidth - containerWidth) / 2;
        if (width > 1140 && trendz_settings.map_address == 'on') {
            $('.map-address-container').css({ left: left + "px" });
        }
        var mapOptions = {
            center: new google.maps.LatLng(trendz_settings.latitude, trendz_settings.longitude),
            height: 400,
            zoom: 18,
            mapMaker: true,
            scrollwheel: false,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false,
            panControl: false,
            styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
        };
        if ($('#map-area').hasClass('map-area')) {
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(trendz_settings.latitude, trendz_settings.longitude),
                animation: google.maps.Animation.DROP,
                map: map,
                title: trendz_settings.map_title
            });
        }
    })();

    $(window).load(function(){
        
        $( '#tz-color-chooser' ).iris({
            width: 200,
            hide: false,
            palettes: ['#e3ae1e', '#6651c2', '#93aeda', '#e8d6be', '#ef6262', '#22854a', '#3c63a9', '#875688' ]
        });

        $( '.color-spinner' ).click( function(){
            $( '.color-chooser' ).toggleClass( 'active' );
        });

        $( '.tz-change-color' ).click( function(){
            event.preventDefault();
            $.cookie("trendz_color",$( '#tz-color-chooser' ).val(), {expires: 7, path: '/wp/trendz'});
            location.reload();
        });

        $('.lazy-load').fadeOut(1000, function(){
            $(this).remove();
        });
    });

});