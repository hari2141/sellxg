jQuery(document).ready(function($) {

    'use strict';

    $(".anim-slider").animateSlider({
        autoplay: true,
        interval: 8000,
        animations: {
            0: {
                "#image-one": {
                    show: "bounceInLeft",
                    hide: "flipOutX",
                    delayShow: "delay1s"
                },
                "#title-one": {
                    show: "bounceInLeft",
                    hide: "fadeOutDownBig",
                    delayShow: "delay1-5s"
                },
                "#text-one": {
                    show: "bounceInLeft",
                    hide: "fadeOutRightBig",
                    delayShow: "delay2s"
                },
                "#btn-one": {
                    show: "bounceInLeft",
                    hide: "fadeOutLeftBig",
                    delayShow: "delay2-5s"
                }
            },
            1: {
                "#image-two": {
                    show: "fadeIn",
                    hide: "fadeOut",
                    delayShow: "delay0-5s"
                },
                "#title-two": {
                    show: "bounceIn",
                    hide: "bounceOut",
                    delayShow: "delay2s"
                },
                "#text-two": {
                    show: "bounceInDown",
                    hide: "bounceOutLeft",
                    delayShow: "delay2-5s"
                },
                "#btn-two": {
                    show: "bounceInRight",
                    hide: "bounceOutRight",
                    delayShow: "delay3s"
                }
            },
            2: {
                "#image-three": {
                    show: "flipInY",
                    hide: "flipOutY",
                    delayShow: "delay0-5s"
                },
                "#title-three": {
                    show: "flipInY",
                    hide: "flipOutY",
                    delayShow: "delay0-5s"
                },
                "#text-three": {
                    show: "bounceIn",
                    hide: "flipOutY",
                    delayShow: "delay1-5s"
                },
                "#btn-three": {
                    show: "rollIn",
                    hide: "flipOutY",
                    delayShow: "delay2s"
                }
            },
            3: {
                "#image-four": {
                    show: "bounceInUp",
                    hide: "fadeOut",
                    delayShow: "delay0-5s"
                },
                "#title-four": {
                    show: "bounceIn",
                    hide: "bounceOut",
                    delayShow: "delay2s"
                },
                "#text-four": {
                    show: "bounceInUp",
                    hide: "bounceOutLeft",
                    delayShow: "delay2-5s"
                },
                "#btn-four": {
                    show: "bounceInRight",
                    hide: "bounceOutRight",
                    delayShow: "delay3s"
                }
            }
        }
    });

});
