<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	public function create_user($name,$mobile,$password_hash,$createddate) {
		
		$data = array(
			'name' => $name,
			'mobile'   => $mobile,
			'password'   => $password_hash,
			'createddate' => $createddate,
		);
		return $this->db->insert('test_seller_reg', $data);
	}
	
	public function verify_user($mobile){
			
		$this->db->select('*');
		$this->db->from('test_seller_reg');
		$this->db->where('mobile', $mobile);	
			
		return $this->db->get()->row('mobile');	
	}
	
		public function resolve_user_login($mobile, $password_hash) {
		
		$this->db->select('*');
		$this->db->from('test_seller_reg');
		$this->db->where('mobile', $mobile);
		$this->db->where('password', $password_hash);
		
		return $this->db->get()->row();
		
	}
	
	public function resolve_user_delete($mobile) {
		
		
		return $this->db->delete("test_seller_reg", "mobile='$mobile'");
		
	}
	 
	public function resolve_user_update($id,$name,$mobile,$password_hash) {
		
			$data = array(
			'id' => $id,
			'name'=>$name,
			'mobile'   => $mobile,
			'password'   => $password_hash,
		);
			 
			$this->db->set($data); 
			$this->db->where("id", $id); 
			$this->db->update("test_seller_reg", $data);

	} 
	public function get_user_from_username($mobile) {
		
		$this->db->from('test_seller_reg');
		$this->db->where('mobile', $mobile);

		return $this->db->get()->row('id');
		
	}
	

	public function get_user($user_id) {
		
		$this->db->from('test_seller_reg');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();
		
	}
	
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}
	
	public function user_sync() {
		
		$query = $this->db->query("select * from test_seller_reg");
		$syncarray=$query->result();
	
		return $syncarray;
	}
	
}
