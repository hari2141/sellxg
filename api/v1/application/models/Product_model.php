<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	public function check_serverproductid($serverProductId) {
		
		$this->db->from('test_product1');
		$this->db->where('serverProductId', $serverProductId);
		return $this->db->get()->row('serverProductId');
		
		}
		
	public function check_serversellerid($serverSellerId){
		
		$this->db->select('*');		
		$this->db->from('test_seller');
		$this->db->where('serverSellerId', $serverSellerId);
		return $this->db->get()->row('serverSellerId');
	}
	
	public function product_insert($productName,$userId,$serverUserId,$category,$subCategory,$type,$sampling,$quantity,$deffectedQuantity,$description,$defect,$moq,$rate,$price,$lotPrice,$lotRate,$numOfDesign,$isSynced,$manufacturingDate,$serverSellerId,$createdDate,$modifiedDate){
		
		$data = array(
			'productName' => $productName,
			'userId'=>$userId,
			'serverUserId'   => $serverUserId,
			'category'   => $category,
			'subCategory' => $subCategory,
			'type'=>$type,
			'sampling'   => $sampling,
			'quantity'   => $quantity,
			'deffectedQuantity' => $deffectedQuantity,
			'description'=>$description,
			'defect'   => $defect,
			'moq'   => $moq,
			'rate' => $rate,
			'type'=>$type,
			'price'   => $price,
			'lotPrice'   => $lotPrice,
			'lotRate'=>$lotRate,
			'numOfDesign'   => $numOfDesign,
			'isSynced'   => $isSynced,
			'manufacturingDate' => $manufacturingDate,
			'serverSellerId'=>$serverSellerId,
			'createdDate'   => $createdDate,
			'modifiedDate'   => $modifiedDate,
		);
		
		$this->db->insert('test_product1', $data);
		return $id = $this->db->insert_id();

	}
	
	public function product_update($productserverId,$productName,$userId,$serverUserId,$category,$subCategory,$type,$sampling,$quantity,$deffectedQuantity,$description,$defect,$moq,$rate,$price,$lotPrice,$lotRate,$numOfDesign,$isSynced,$manufacturingDate,$serverSellerId,$createdDate,$modifiedDate){
		
		$data = array(
			'productName' => $productName,
			'userId'=>$userId,
			'serverUserId'   => $serverUserId,
			'category'   => $category,
			'subCategory' => $subCategory,
			'type'=>$type,
			'sampling'   => $sampling,
			'quantity'   => $quantity,
			'deffectedQuantity' => $deffectedQuantity,
			'description'=>$description,
			'defect'   => $defect,
			'moq'   => $moq,
			'rate' => $rate,
			'type'=>$type,
			'price'   => $price,
			'lotPrice'   => $lotPrice,
			'lotRate'=>$lotRate,
			'numOfDesign'   => $numOfDesign,
			'isSynced'   => $isSynced,
			'manufacturingDate' => $manufacturingDate,
			'serverSellerId'=>$serverSellerId,
			'createdDate'   => $createdDate,
			'modifiedDate'   => $modifiedDate,
		);
		$this->db->where('serverProductId','$serverProductId');
		$this->db->update('test_product1',$data);
		return $productserverId;

	}
	
	public function get_categoryid($category)
	{	
		$this->db->select('*');
		$this->db->from('test_category');
		$this->db->where('category', $category);

		return $this->db->get()->row('categoryId');
		
	}
	
	
	public function get_category_attribute($categoryid){
		
			
		$this->db->select('*');
		$this->db->from('test_Category_attribute');
		$this->db->where('catid', $categoryid);
		return $this->db->get()->row();
	
	}
	
	
	public function get_category_attributearray($categoryid){
		
		$query = $this->db->query("select * from test_Category_attribute where catid='".$categoryid."'");
		$categoryarray = array();
		$i=0;
		foreach($query->result() as $row)
		{
			$categoryarray[$i] = $row->Attr_name;
			$i++;
		}
		return $categoryarray;
	}
	public function insert_category_attribute($serverProductId,$atr_name,$attr_value)
	{
			$data=array(
				'Prdid'   => $serverProductId,
				'Attr_name'   => $atr_name,
				'Attr_value'  => $attr_value,
			);
		
		 return $this->db->insert('test_product_attribute', $data);	
	} 
	
	public function update_category_attribute($serverproductId,$atr_name,$attr_value)
	{
			$data=array(
				'Attr_name'   => $atr_name,
				'Attr_value'  => $attr_value,
			);
		$this->db->where('Prdid','$serverproductId');
		return $this->db->update('test_product_attribute',$data);
	} 
	
	public function insert_product_image($serverProductId,$imageName,$fpath)
	{
			$data=array(
				'serverProductId'   => $serverProductId,
				'imageName'   => $imageName,
				'image'  => $fpath,
			);
		 return $this->db->insert('test_image',$data);	
	}

	public function update_product_image($serverProductId,$imageName,$fpath)
	{
			$data=array(
				'serverProductId'   => $serverProductId,
				'imageName'   => $imageName,
				'image'  => $fpath,
			);
		$this->db->where('serverProductId','$serverproductId');
		return $this->db->update('test_image',$data);	
	}
	
	public function get_category_attributecount($categoryid){
		
		$this->db->select('*');
		$this->db->from('test_Category_attribute');
		$this->db->where('catid', $categoryid);
		return $this->db->get()->num_rows();		
		
	}
	
	
}
