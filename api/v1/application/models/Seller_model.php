<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller_model extends CI_Model {

	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	public function create_seller($sellerId,$sellerName,$userId,$serverUserId,$shopName,$address1,$address2,$city,$state,$country,$emailId,$mobileNoPrimary,$mobileNoSecondary,$landlineNo,$shopLicensepath,$registrationNo,$visitingCard1path,$visitingCard2path,$profilePicpath,$pancardNo,$pancardPicpath,$cstNo,$sstNo,$bankName,$accountNo,$ifscCode,$branchName,$isSynced,$createdDate,$modifiedDate) 
	{	
		$data = array(
			'sellerId' => $sellerId,
			'sellerName'   => $sellerName,
			'userId'   => $userId,
			'serverUserId' => $serverUserId,
			'shopName' => $shopName,
			'address1'   => $address1,
			'address2'   => $address2,
			'city' => $city,
			'state' => $state,
			'country'   => $country,
			'emailId' => $emailId,
			'mobileNoPrimary' => $mobileNoPrimary,
			'mobileNoSecondary'   => $mobileNoSecondary,
			'landlineNo'   => $landlineNo,
			'shopLicense'   => $shopLicensepath,
			'registrationNo' => $registrationNo,
			'visitingCard1' => $visitingCard1path,
			'visitingCard2'   => $visitingCard2path,
			'pancardNo'   => $pancardNo,
			'profilePic' => $profilePicpath,
			'pancardPic' => $pancardPicpath,
			'cstNo'   => $cstNo,
			'sstNo'   => $sstNo,
			'bankName' => $bankName,
			'accountNo'   => $accountNo,
			'ifscCode' => $ifscCode,
			'branchName' => $branchName,
			'isSynced'   => $isSynced,
			'createdDate'   => $createdDate,
			'modifiedDate' => $modifiedDate,
		);
		
		 $this->db->insert('test_seller', $data);
		 return $insert_id = $this->db->insert_id();
		 
		}
	
	
	public function check_sync($synctime) {
		
		$query = $this->db->query("select * from test_seller where modifiedDate>$synctime");
		$syncarray=$query->result();
		return $syncarray;
	}
	
	public function get_seller_from_username($user_id) {
		
		$this->db->from('test_seller');
		$this->db->where('serverSellerId', $user_id);
		return $this->db->get()->row('serverSellerId');	
	}
	
	public function get_user($user_id) {
	
		$this->db->from('test_seller_reg');
		$this->db->where('id', $user_id);
		return $this->db->get()->row();	
	}
}
