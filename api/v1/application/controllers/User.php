<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {


	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('user_model');	
	}
	
	
	public function index() {
	echo "Welcome to SellXg";
	}
	
	public function register() 
	{
			$createddate = round(microtime(true)*1000);
			
			 $name = $this->input->post('name');
			 $mobile = $this->input->post('mobile');
			 $password_hash = $this->input->post('password');
			 
			if(!empty($mobile) and !empty($password_hash))
			{
			if($this->user_model->verify_user($mobile))
			{
				$emp['status_code']='0';  
				$emp['message']="Username already inserted";
				$response=$emp;
				echo json_encode($response);
			}else{
			
			if ($this->user_model->create_user($name,$mobile,$password_hash,$createddate)) {
				
				$emp['status_code']='1';  
				$emp['message']="register successfully";
				$response=$emp;
				echo json_encode($response);
				
			} else {
				
				$emp['status_code']='0';  
				$emp['message']="registration failed";
				$response=$emp;
				echo json_encode($response);
				
				
			}
			}
			}else{
				$emp['status_code']='0';  
				$emp['message']="Missing parameter ";
				$response=$emp;
				echo json_encode($response);
			}
			
		
		
	}
		
	
	public function login() {
		
		
			$mobile = $this->input->post('mobile');
			$password_hash = $this->input->post('password');
			
			
			if(!empty($mobile) and !empty($password_hash))
			{
			if ($this->user_model->resolve_user_login($mobile, $password_hash)) {
				
				$user_id = $this->user_model->get_user_from_username($mobile);
				
				$emp['userid']=$user_id;
			
				
				$emp['status_code']='1';  
				$emp['message']="login successfully";
				$response=$emp;
				echo json_encode($response);
				
				
			} else {
				$emp['status_code']='0';  
				$emp['message']="Invalid email or password!";
				$response=$emp;
				echo json_encode($response);
				
			}
			}else{
				$emp['status_code']='0';  
				$emp['message']="Missing parameter ";
				$response=$emp;
				echo json_encode($response);
			}
			
	
		
	}
	 
	
	public function sync() {
		
		
				$userdata=$this->user_model->user_sync();
			
				$i=0;
				$user = array();
				foreach($userdata as $udata => $data)
				{
				$user[$i]['serverUserId'] = $data->id;
				$user[$i]['name'] = $data->name;
				$user[$i]['mobileNo'] = $data->mobile;
				$user[$i]['password'] = $data->password;
				$i++;
				}
				$emp['status_code']='1';  
				$emp['message']="User Record";
				$emp['user']=$user;
				$response=$emp;
				echo json_encode($response); 
		
	}
	
	public function response(){
		
							}
}