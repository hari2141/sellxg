<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product class.
 * 
 * @extends CI_Controller
 */
class Product extends CI_Controller {

	
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('product_model');
		
	}
	
	
	public function index() {
		echo "welcome to sellXg";	
		}
	
	
	public function add() {
		
		
			$createdDate = $this->input->post('createdDate');
			$modifiedDate = $this->input->post('modifiedDate');
			
			$createdDate = round(microtime($createdDate)*1000);
			$modifiedDate = round(microtime($modifiedDate)*1000);
			
			$serverProductId = $this->input->post('serverProductId');
			$userId = $this->input->post('userId');
			$serverUserId = $this->input->post('serverUserId');
			$productName = $this->input->post('productName');
			$category = $this->input->post('category');
			$subCategory = $this->input->post('subCategory');
			$type = $this->input->post('type');
			$sampling = $this->input->post('sampling');
			$quantity = $this->input->post('quantity');
			$manufacturingDate = $this->input->post('manufacturingDate');
			$description = $this->input->post('description');
			$deffectedQuantity = $this->input->post('deffectedQuantity');
			$defect = $this->input->post('defect');
			$rate = $this->input->post('rate');
			$price = $this->input->post('price');
			$lotPrice = $this->input->post('lotPrice');
			$lotRate = $this->input->post('lotRate');
			$moq = $this->input->post('moq');
			$numOfDesign = $this->input->post('numOfDesign');
			$isSynced = $this->input->post('isSynced');
			
			$sellerId = $this->input->post('sellerId');
			$serverSellerId = $this->input->post('serverSellerId');
			
			$count = $this->input->post('count');
			
			if(!empty($sellerId) and !empty($serverSellerId))
			{
				$sellerserverId=$this->product_model->check_serversellerid($serverSellerId);
				if($sellerserverId===$serverSellerId)
				{
				$productserverId=$this->product_model->check_serverproductid($serverProductId);
				
					if(empty($productserverId))
					{
				
					$ProductServerId=$this->product_model->product_insert($productName,$userId,$serverUserId,$category,$subCategory,$type,$sampling,$quantity,$deffectedQuantity,$description,$defect,$moq,$rate,$price,$lotPrice,$lotRate,$numOfDesign,$isSynced,$manufacturingDate,$serverSellerId,$createdDate,$modifiedDate);
				
					$serverproductId=$ProductServerId;
					$selarray['serverProductId'] = $serverproductId;
				
					$categoryid=$this->product_model->get_categoryid($category);
					$category_attribute=$this->product_model->get_category_attributearray($categoryid);
					$category_attributecount=$this->product_model->get_category_attributecount($categoryid);
					$attribut_name=array();
					$i=0;
					
					foreach($category_attribute as $categorydata){
					     $attribute_name[$i]=$categorydata;
						
						$atr_name=$attribute_name[$i];
						$atr_value[$i]=$this->input->post($attribute_name[$i]); 
						$attr_value=$atr_value[$i];
						$i++;
						}
						$j=0;
						for($j=0; $j<$i; $j++){
						$prdattr =$this->product_model->insert_category_attribute($serverproductId,$atr_name,$attr_value);
						}
						for($i=1; $i<=$count; $i++){
							$image = $_FILES['image_'.$i]['name'];
							
							$imageName="image_$i";
							$path = "../sellxg/admin/upload/";
							$fpath = time()."_".$image;
							$this->product_model->insert_product_image($serverproductId,$imageName,$fpath);
						
						}
						$emp['status_code'] = "1";
						$emp['message'] = "Record Inserted Successfully";
						$emp['products'] = $selarray;
						$response = $emp;
						echo json_encode($response);
					}else{
					
					$emp['status_code'] = "0";
					$emp['message'] = "product server id is null";
					$response = $emp;
					echo json_encode($response);
					}
				}else{
					
					$selarray['sellerId'] = $sellerId;
					$selarray['serverSellerId'] = $serverSellerId;
					$emp['products'] = $selarray;
					
					$emp['status_code'] = "2";
					$emp['message'] = "server Seller ID not Found";
					$response = $emp;
					echo json_encode($response);
					}
			}else{
					$emp['status_code'] = "0";
					$emp['message'] = "Missing Parameter!";
					$response = $emp;
					echo json_encode($response);
			}
	}	
	
	
	
	public function update() {
		
		
			$createdDate = $this->input->post('createdDate');
			$modifiedDate = $this->input->post('modifiedDate');
			
			$createdDate = round(microtime($createdDate)*1000);
			$modifiedDate = round(microtime($modifiedDate)*1000);
			
			$serverProductId = $this->input->post('serverProductId');
			$userId = $this->input->post('userId');
			$serverUserId = $this->input->post('serverUserId');
			$productName = $this->input->post('productName');
			$category = $this->input->post('category');
			$subCategory = $this->input->post('subCategory');
			$type = $this->input->post('type');
			$sampling = $this->input->post('sampling');
			$quantity = $this->input->post('quantity');
			$manufacturingDate = $this->input->post('manufacturingDate');
			$description = $this->input->post('description');
			$deffectedQuantity = $this->input->post('deffectedQuantity');
			$defect = $this->input->post('defect');
			$rate = $this->input->post('rate');
			$price = $this->input->post('price');
			$lotPrice = $this->input->post('lotPrice');
			$lotRate = $this->input->post('lotRate');
			$moq = $this->input->post('moq');
			$numOfDesign = $this->input->post('numOfDesign');
			$isSynced = $this->input->post('isSynced');
			
			$sellerId = $this->input->post('sellerId');
			$serverSellerId = $this->input->post('serverSellerId');
			
			$count = $this->input->post('count');
			
			if(!empty($sellerId) and !empty($serverSellerId))
			{
				$sellerserverId=$this->product_model->check_serversellerid($serverSellerId);
				if($sellerserverId===$serverSellerId)
				{
				$productserverId=$this->product_model->check_serverproductid($serverProductId);
				
					if(!empty($productserverId))
					{
				
					$ProductServerId=$this->product_model->product_update($productserverId,$productName,$userId,$serverUserId,$category,$subCategory,$type,$sampling,$quantity,$deffectedQuantity,$description,$defect,$moq,$rate,$price,$lotPrice,$lotRate,$numOfDesign,$isSynced,$manufacturingDate,$serverSellerId,$createdDate,$modifiedDate);
				
					$serverproductId=$ProductServerId;
					$selarray['serverProductId'] = $serverproductId;
				
					$categoryid=$this->product_model->get_categoryid($category);
					$category_attribute=$this->product_model->get_category_attributearray($categoryid);
					$category_attributecount=$this->product_model->get_category_attributecount($categoryid);
					$attribut_name=array();
					$i=0;
					
					foreach($category_attribute as $categorydata){
					     $attribute_name[$i]=$categorydata;
						
						$atr_name=$attribute_name[$i];
						$atr_value[$i]=$this->input->post($attribute_name[$i]); 
						$attr_value=$atr_value[$i];
						$i++;
						}
						$j=0;
						for($j=0; $j<$i; $j++){
						$prdattr =$this->product_model->update_category_attribute($serverproductId,$atr_name,$attr_value);
						}
						for($i=1; $i<=$count; $i++){
							$image = $_FILES['image_'.$i]['name'];
							
							$imageName="image_10";
							$path = "../sellxg/admin/upload/";
							$fpath = time()."_".$image;
							$this->product_model->update_product_image($serverproductId,$imageName,$fpath);
						
						}
						$emp['status_code'] = "1";
						$emp['message'] = "Record Updated Successfully";
						$emp['products'] = $selarray;
						$response = $emp;
						echo json_encode($response);
					}else{
					
					$emp['status_code'] = "0";
					$emp['message'] = "product server id is null";
					$response = $emp;
					echo json_encode($response);
					}
				}else{
					
					$selarray['sellerId'] = $sellerId;
					$selarray['serverSellerId'] = $serverSellerId;
					$emp['products'] = $selarray;
					
					$emp['status_code'] = "0";
					$emp['message'] = "server Seller ID not Found";
					$response = $emp;
					echo json_encode($response);
					}
			}else{
					$emp['status_code'] = "0";
					$emp['message'] = "Missing Parameter!";
					$response = $emp;
					echo json_encode($response);
			}
	}
	
	
		public function response(){
		
		}
}
