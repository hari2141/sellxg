<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Seller class.
 * 
 * @extends CI_Controller
 */
class Seller extends CI_Controller {


	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('seller_model');	
	}
	
	
	public function index() {
	echo "Welcome to SellXg";
	}
	
	public function register() 
	{
			$path =  "../sellxg/admin/upload/";
			$createdDate = $this->input->post('createdDate');
			$modifiedDate = $this->input->post('modifiedDate');
			$shopLicense = "";
			
			$createdDate = round(microtime($createdDate)*1000);
			$modifiedDate = round(microtime($modifiedDate)*1000);
			
			$sellerId = $this->input->post('sellerId');
			$userId = $this->input->post('userId');
			$shopName = $this->input->post('shopName');
			$sellerName = $this->input->post('sellerName');
			$shopName = $this->input->post('shopName');
			$serverUserId = $this->input->post('serverUserId');
			$address1 = $this->input->post('address1');
			$address2 = $this->input->post('address2');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$country = $this->input->post('country');
			$emailId = $this->input->post('emailId');
			$mobileNoPrimary = $this->input->post('mobileNumberPrimary');
			$mobileNoSecondary = $this->input->post('mobileNumberSecondary');
			$landlineNo = $this->input->post('landLine');
			
			$registrationNo = $this->input->post('registrationNo');
			$landlineNo = $this->input->post('landLine');
			$pancardNo = $this->input->post('pancardNo');
			
			$shopLicense = $_FILES['shopLicense']['name'];
				if($shopLicense==""){
					$shopLicensepath="";
				}else{
					$shopLicensepath=time()."_".$shopLicense;
				}
				
				
				$visitingCard1 = $_FILES['visitingCard1']['name'];
				if($visitingCard1==""){
					$visitingCard1path="";
				}else{
					$visitingCard1path=time()."_".$visitingCard1;
				}
				
				
				$visitingCard2 = $_FILES['visitingCard2']['name'];
				if($visitingCard2==""){
					$visitingCard2path="";
				}else{
					$visitingCard2path=time()."_".$visitingCard2;
				}
				
				
				$profilePic = $_FILES['profilePic']['name'];
				if($profilePic==""){
					$profilePicpath="";
				}else{
					$profilePicpath=time()."_".$profilePic;
				}
				
				
				$pancardPic = $_FILES['pancardPic']['name'];
				if($pancardPic==""){
					$pancardPicpath="";
				}else{
					$pancardPicpath=time()."_".$pancardPic;
				}
				
			$cstNo = $this->input->post('cstNo');
			$sstNo = $this->input->post('sstNo');
			$bankName = $this->input->post('bankName');
			$accountNo = $this->input->post('accountNo');
			$ifscCode = $this->input->post('ifscCode');
			$branchName = $this->input->post('branchName');
			$isSynced = $this->input->post('isSynced');
			
			if(empty($sellerId) and empty($shopName) and empty($mobileNoPrimary) and empty($address1))
			{
				$emp['status_code']='0';  
				$emp['message']="Missing Parameter";
				$response=$emp;
				echo json_encode($response);
			}
			else{
				
			$user_id=$this->seller_model->create_seller($sellerId,$sellerName,$userId,$serverUserId,$shopName,$address1,$address2,$city,$state,$country,$emailId,$mobileNoPrimary,$mobileNoSecondary,$landlineNo,$shopLicensepath,$registrationNo,$visitingCard1path,$visitingCard2path,$profilePicpath,$pancardNo,$pancardPicpath,$cstNo,$sstNo,$bankName,$accountNo,$ifscCode,$branchName,$isSynced,$createdDate,$modifiedDate);
			if(!empty($user_id))
			{
				$sellerid = $this->seller_model->get_seller_from_username($user_id);
				
				$seller['sellerId']=$sellerId;
				$seller['serversellerId']=$sellerid;
				$emp['seller']=$seller;
				$emp['status_code']='1';  
				$emp['message']="register successfully";
				$response=$emp;
				echo json_encode($response);
				
			}
			}
	}
		
		
		
		
	public function sync() 
	{
			
			$synctime = $this->input->post('synctime');
			
			if(!empty($synctime))
			{
				
				$sellerdata=$this->seller_model->check_sync($synctime);
				 	
					$i=0;
					 foreach($sellerdata as $data1 => $data)
					{
				$seller[$i]['sellerId'] = $data->sellerId;
				$seller[$i]['serverSellerId'] = $data->serverSellerId;
				$seller[$i]['userId'] = $data->userId;
				$seller[$i]['serverUserId'] = $data->serverUserId;
				$seller[$i]['sellerName'] = $data->sellerName;
				$seller[$i]['shopName'] = $data->shopName;
				$seller[$i]['address1'] = $data->address1;
				$seller[$i]['address2'] = $data->address2;
				$seller[$i]['city'] = $data->city;
				$seller[$i]['state'] = $data->state;
				$seller[$i]['country'] = $data->country;
				$seller[$i]['emailId'] = $data->emailId;
				$seller[$i]['mobileNoPrimary'] = $data->mobileNoPrimary;
				$seller[$i]['mobileNoSecondary'] = $data->mobileNoSecondary;
				$seller[$i]['landlineNo'] = $data->landlineNo;
				
				
				if($data->shopLicense == ""){
					$seller[$i]['shopLicense'] = "";
				}else{
					$seller[$i]['shopLicense'] = "http://creadigol.biz/sellxg/sellxg/admin/upload/".$data->shopLicense;
				}
				
				if($data->visitingCard1 == ""){
					$seller[$i]['visitingCard1'] = "";
				}else{
					$seller[$i]['visitingCard1'] = "http://creadigol.biz/sellxg/sellxg/admin/upload/".$data->visitingCard1;
				}
				
				if($data->visitingCard2 == ""){
					$seller[$i]['visitingCard2'] = "";
				}else{
					$seller[$i]['visitingCard2'] = "http://creadigol.biz/sellxg/sellxg/admin/upload/".$data->visitingCard2;
				}
				
				if($data->profilePic == ""){
					$seller[$i]['profilePic'] = "";
				}else{
					$seller[$i]['profilePic'] = "http://creadigol.biz/sellxg/sellxg/admin/upload/".$data->profilePic;
				}
				
				if($data->pancardPic == ""){
					$seller[$i]['pancardPic'] = "";
				}else{
					$seller[$i]['pancardPic'] = "http://creadigol.biz/sellxg/sellxg/admin/upload/".$data->pancardPic;
				} 
				
				$seller[$i]['registrationNo'] = $data->registrationNo;
				$seller[$i]['pancardNo'] = $data->pancardNo;
				$seller[$i]['cstNo'] = $data->cstNo;
				$seller[$i]['sstNo'] = $data->sstNo;
				$seller[$i]['bankName'] = $data->bankName;
				$seller[$i]['accountNo'] = $data->accountNo;
				$seller[$i]['ifscCode'] = $data->ifscCode;
				$seller[$i]['branchName'] = $data->branchName;
				$seller[$i]['isSynced'] = $data->isSynced;
				$seller[$i]['createdDate'] = date("d-m-Y",$data->createdDate/1000);
				$seller[$i]['modifiedDate'] = date("d-m-Y",$data->modifiedDate/1000);
						$i++;
					}
					
					$emp['status_code']='1';  
					$emp['message']=" Seller Record";
					$emp['sellers']=$seller;
					$response=$emp;
					echo json_encode($response);
			}else{
					$emp['status_code']='0';  
					$emp['message']=" Missing Parameter";
					$response=$emp;
					echo json_encode($response);
			}
	}	
		
	public function response(){
		
							}
}