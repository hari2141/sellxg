<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
	require_once('db.php');
	require_once('head.php');
	require_once('usercondition.php');
	$up=0;
	if(isset($_REQUEST['subbtn']))
	{
		
		$up=mysql_query("update ".USER_REGISTER ." set user_name='$_REQUEST[username]',gender='$_REQUEST[gender]' where user_id=$_SESSION[userid]");
		$_SESSION['username']=$_REQUEST['username'];
		$_SESSION['gender']=$_REQUEST['gender'];
	}
 ?>

<body>

<?php
	require_once('tophead.php');
 ?>
<header class="nav-slider">
<?php

	require_once('menu.php');
 ?>
  <div class="row">
      <div class="col-md-12">
        <div class="page-location" ><strong></strong><a href="index.php">Home</a><span>>></span><a href="#">Review</a></div>
        <div class="mr-1000 mr-1000-location"></div>
      </div>
    </div>
</header>
<div class="row mainaccountdiv">
		<div class="col-md-1">
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12">
		<?php
			$sel=mysql_query("select * from ".PRODUCT ." where serverProductId=$_REQUEST[pid]");
			$fet=mysql_fetch_array($sel);
			$selimg=mysql_query("select * from ".IMAGE ." where serverProductId=$_REQUEST[pid] limit 0,1");
			$fetimg=mysql_fetch_array($selimg);
		?>
			<div class="col-md-3 myaccountnav" style="border:1px solid #E3AE1E;">
				<div class="row" style="padding:4px;background:#E3AE1E;text-align:left;font-size:20px;color:#fff;">
					<font>&nbsp;&nbsp;Your Review</font>
				</div>
			
				<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px 0px 10px 0px;font-weight:bold;border-bottom:1px solid #ddd;">
					<font style="color:#222;font-size:14px;">You Have Chosen To Review</font><br>
				</div>
				<div class="col-md-12 col-xs-12 col-sm-12" style="padding:5px 0px 10px 0px;">
					<Br><div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">	
						<a href="product details.php?<?php echo $_REQUEST[pid]; ?>"><img src="images/<?php echo $fetimg[3];?>" style="height:200px;width:100%;" /></a>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;margin-top:10px;">	
						<a href="product details.php?<?php echo $_REQUEST[pid]; ?>" style="color:#222;text-transform:capitalize;"><?php echo $fet[3]; ?></a>
					</div>
				</div>
				<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px;font-weight:bold;border:1px solid #eee;">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<font style="color:#222;font-size:12px;">What makes a good review</font><br>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Have you used this product?</font>
						<font style="color:#555;font-size:10px;">It's always better to review a product you have personally experienced.</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Educate your readers</font><br>
						<font style="color:#555;font-size:10px;">Provide a relevant, unbiased overview of the product. Readers are interested in the pros and the cons of the product.</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Be yourself, be informative</font><br>
						<font style="color:#555;font-size:10px;">Let your personality shine through, but it's equally important to provide facts to back up your opinion.</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Get your facts right!</font><br>
						<font style="color:#555;font-size:10px;">Nothing is worse than inaccurate information. If you're not really sure, research always helps.</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Stay concise</font><br>
						<font style="color:#555;font-size:10px;">Be creative but also remember to stay on topic. A catchy title will always get attention!</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:10px;text-align:justify;">
						<font style="color:#111;font-size:11px;">Easy to read, easy on the eyes</font><br>
						<font style="color:#555;font-size:10px;">A quick edit and spell check will work wonders for your credibility. Also, break reviews into small, digestible paragraphs.</font>
					</div>
				</div>
			</div>
			<div class="col-md-9 myaccountinfo">
				<div class="col-sm-12 col-md-12 col-xs-12" style="border:1px solid #E3AE1E;">
					<div class="row" style="padding:9px;background:#E3AE1E;text-align:left;font-size:15px;color:#fff;">
						<font style="letter-spacing:1px;">
							Help others! Write a sellXG review
						</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:15px;">
						<form action="" method="post">
							<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px 0px;border-bottom:1px solid #eee;">	
								<h5>Guidelines for writing a product review All fields are mandatory</h5>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 logintext" style="margin:10px 0px;">	
								<div class="col-md-12" style="text-align:left;margin-left:1px;">
									<lable style="font-size:13px;">Review Title  <font style="color:red;font-size:16px;">*</font></lable>
								</div>
								<div class="col-md-12">
									<input type="text" class="form-control" name="reviewtitle" required />
								</div>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 logintext">	
								<div class="col-md-12" style="text-align:left;margin-left:1px;">
									<lable style="font-size:13px;">Your Review  <font style="color:red;font-size:16px;">*</font></lable>
								</div>
								<div class="col-md-12 col-xs-12 col-sm-12">
									<input type="text" class="form-control" name="review" required />
								</div>
								
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12" style="margin:10px 0px;">		
									<button type="submit" name="subbtn" style="color:white;font-size:12px;margin-right:10px;background:#E3AE1E;border:1px solid #fff;padding:10px;letter-spacing:1px;"> SAVE CHANGES</button>			
								</div>
						</form>	
					</div>
					<?php
					if($up==1)
					{
						?>
						<div class="col-md-push-3 col-md-6 col-sm-12 col-xs-12 animated jello" style="padding:10px; background:#E3AE1E;text-align:center;margin-bottom:20px;">
							<font style="color:#fff;font-size:13px;">Your Account Successfully Updated...</font>
						</div>
						<?php
					}
					?>
					
				</div>
			</div>
		</div>
		<div class="col-md-1">
		</div>
</div>
<!--<div class="lazy-load">

  <div class='Cube panelLoad'>
    <div class='cube-face cube-face-front'>S</div>
    <div class='cube-face cube-face-back'>E</div>
    <div class='cube-face cube-face-left'>L</div>
    <div class='cube-face cube-face-right'>L</div>
    <div class='cube-face cube-face-bottom'>X</div>
    <div class='cube-face cube-face-top'>G</div>
  </div>
</div>-->
<!----footer ----->
<?php
	require_once('footer.php');
?>
</body>

<!--- Vijay add JS all is working Site ---->

<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/scrollReveal.min.js'></script>
<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/js'></script>
<!--<script type='text/javascript' src='http://www.themecop.com/wp/trendz/wp-content/themes/trendz/js/jquery.animateSlider.min.js?ver=4.5.2'></script>-->
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/core.min.js'></script>
<script type='text/javascript' src='js/widget.min.js'></script>
<script type='text/javascript' src='js/mouse.min.js'></script>
<script type='text/javascript' src='js/draggable.min.js?'></script>
<script type='text/javascript' src='js/slider.min.js'></script>
<script type='text/javascript' src='js/iris.min.js?'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var trendz_settings = {"latitude":"-37.8173306","longitude":"144.9556518","map_address":"on","map_title":"Envato","loading_time":"40000"};
/* ]]> */
</script>
<script type='text/javascript' src='js/main.js'></script>
</html>

