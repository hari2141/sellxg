<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
		$this->load->helper(array('url'));
		$this->load->model('Seller_model');
		
	}
	
	public function index()
	{
		echo "sellxg api to insert, update and sync seller";
	}
	public function insert()
	{
		$name = $this->input->post('name');
		
		if(!empty($name)){
			if($this->Seller_model->create_user($name)){
				$res['error'] = false;
				$res['name'] = $name;
				echo json_encode($res);
			}
		}else{
			$res['error'] = true;
			$res['message'] = "required parameter like name";

			echo json_encode($res);
		}
		
	}
	public function get()
	{
		$name = $this->input->post('name');
		
		if(!empty($name)){
			$val = $this->Seller_model->get_user($name);
			if(!empty($val)){
				
				print_r($val);
				foreach($val as $key=>$data){
					echo $key.'<br>';
					echo $data->name;
					echo "<br>";
				}
				$res['error'] = false;
				$res['name'] = $name;
				echo json_encode($res);
			}
		}else{
			$res['error'] = true;
			$res['message'] = "required parameter like name";

			echo json_encode($res);
		}
		
	}
}
