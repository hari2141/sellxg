/* ------------------------------------------------------------ *\
|* ------------------------------------------------------------ *|
|* Some JS to help with our search
|* ------------------------------------------------------------ *|
\* ------------------------------------------------------------ */
( function( $ ) {
	'use strict';
	// get vars
	var input = $("#input");
	var level = $("#label");

	level.on('click', function(){
		$(this).toggleClass('active');
		input.toggleClass('focus');
	});
	$(document).on('click', function(e){
		var clickedID = e.target.id;
		if (clickedID != "search-terms" && clickedID != "search-label" && clickedID != "search-icon") {
			input.removeClass('focus');
			level.removeClass('active');
		}
	});
} )( jQuery );