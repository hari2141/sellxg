<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
	require_once('db.php');
	require_once('head.php');
	require_once('usercondition.php');
	$up=0;
	$o=0;
	$cn=0;
	$co=0;
	if(isset($_REQUEST['subbtn']))
	{
		
			$up=mysql_query("update ".USER_REGISTER ." set user_contactno='$_REQUEST[mobile]',user_email='$_REQUEST[email]' where user_id=$_SESSION[userid]");
			$_SESSION['email']=$_REQUEST['email'];
			$_SESSION['mobile']=$_REQUEST['mobile'];

	}	
 ?>

<body>

<?php
	require_once('tophead.php');
 ?>
<header class="nav-slider">
<?php

	require_once('menu.php');
 ?>
  <div class="row">
      <div class="col-md-12">
        <div class="page-location" ><strong></strong><a href="index.php">Home</a><span>>></span><a href="myaccount.php">My Account</a><span>>> Change Email/Mobile</span></div>
        <div class="mr-1000 mr-1000-location"></div>
      </div>
    </div>
</header>
<div class="row mainaccountdiv">
		<div class="col-md-1">
		</div>
		<div class="col-md-10 col-sm-12 col-xs-12">
			<?php
				require_once('usermyaccount.php');
			?>
			<div class="col-md-9 myaccountinfo">
				<div class="col-sm-12 col-md-12 col-xs-12" style="border:1px solid #E3AE1E;">
					<div class="row" style="padding:9px;background:#E3AE1E;text-align:left;font-size:15px;color:#fff;">
						<font>
							For your security, we recommend that you strengthen your password. Change your password  Dismiss
						</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:15px;">
						<form action="" method="post">
							<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px 0px;border-bottom:1px solid #eee;">	
								<h4>Update Email/Mobile</h4>
							</div>
							<?php
							if($co==1 || $cn==1 || $o==1)
							{
							?>
								<div class="col-md-6 col-sm-12 col-xs-12 animated jello" style="border:1px solid #b00;padding:7px;color:#b00;background:#fce7e7;text-align:center;margin:20px 0px;">
									<font style="color:#b00;font-size:13px;">
									<?php
										if($co==1)
										{
											echo "Password change failed. New Password same as the old password";
										}
										elseif($cn==1)
										{
											echo "Password change failed. New Passwords do not match";
										}
										elseif($o==1)
										{
											echo "Password change failed. Invalid old password";
										}
									?>
									</font>
								</div>
							<?php
							}
							?>
							<div class="col-md-12 col-xs-12 col-sm-12 logintext" style="margin-top:10px;">	
								<div class="col-md-12" style="text-align:left;margin-left:1px;">
									<lable style="font-size:13px;">Email Address  <font style="color:red;font-size:16px;">*</font></lable>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="email" class="form-control" name="email" value="<?php echo $_SESSION['email'] ?>" required />
								</div>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 logintext">	
								<div class="col-md-12" style="text-align:left;margin-left:1px;">
									<lable style="font-size:13px;">Mobile No  <font style="color:red;font-size:16px;">*</font></lable>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<input type="text" class="form-control" name="mobile" value="<?php echo $_SESSION['mobile'] ?>" required />
								</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12" style="margin:10px 18px;">		
									<button type="submit" name="subbtn" style="color:white;font-size:12px;margin-right:10px;background:#E3AE1E;border:1px solid #fff;padding:10px;letter-spacing:1px;"> SAVE CHANGES</button>			
								</div>
							<div class="col-md-12 col-xs-12 col-sm-12 " style="margin-top:20px;">
							<div class="col-md-12" style="text-align:left;">
							<p style="text-align:justify;">							
								<b>What happens when I update my email address (or mobile number)?</b><br>
									Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).<br><BR>
								<b>When will my SellXG account be updated with the new email address (or mobile number)?</b><br>
									It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.<br><BR>
								<b>What happens to my existing SellXG account when I update my email address (or mobile number)?</b><br>
									Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.<br><BR>
								<b>Does my Seller account get affected when I update my email address?</b><br>
									SellXG has a 'single sign-on' policy. Any changes will reflect in your Seller account also.<br>
							</p>
							</div>
							</div>
								
							</div>
						</form>
<?php
					if($up==1)
					{
						?>
						<div class="col-md-push-3 col-md-6 col-sm-12 col-xs-12 animated jello" style="padding:10px; background:#E3AE1E;text-align:center;margin-bottom:20px;">
							<font style="color:#fff;font-size:13px;">Your Account Successfully Updated...</font>
						</div>
						<?php
					}
					?>						
					</div>
					
					
				</div>
			</div>
		</div>
		<div class="col-md-1">
		</div>
</div>
<!--<div class="lazy-load">

  <div class='Cube panelLoad'>
    <div class='cube-face cube-face-front'>S</div>
    <div class='cube-face cube-face-back'>E</div>
    <div class='cube-face cube-face-left'>L</div>
    <div class='cube-face cube-face-right'>L</div>
    <div class='cube-face cube-face-bottom'>X</div>
    <div class='cube-face cube-face-top'>G</div>
  </div>
</div>-->
<!----footer ----->
<?php
	require_once('footer.php');
?>
</body>

<!--- Vijay add JS all is working Site ---->

<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/scrollReveal.min.js'></script>
<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/js'></script>
<!--<script type='text/javascript' src='http://www.themecop.com/wp/trendz/wp-content/themes/trendz/js/jquery.animateSlider.min.js?ver=4.5.2'></script>-->
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/core.min.js'></script>
<script type='text/javascript' src='js/widget.min.js'></script>
<script type='text/javascript' src='js/mouse.min.js'></script>
<script type='text/javascript' src='js/draggable.min.js?'></script>
<script type='text/javascript' src='js/slider.min.js'></script>
<script type='text/javascript' src='js/iris.min.js?'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var trendz_settings = {"latitude":"-37.8173306","longitude":"144.9556518","map_address":"on","map_title":"Envato","loading_time":"40000"};
/* ]]> */
</script>
<script type='text/javascript' src='js/main.js'></script>
</html>

