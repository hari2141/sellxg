<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
	require_once('db.php');
	require_once('head.php');	
 ?>

<body>

<?php
	require_once('tophead.php');
 ?>
<header class="nav-slider">
<?php

	require_once('menu.php');
 ?>
  <div class="row">
      <div class="col-md-12">
        <div class="page-location" ><strong></strong><a href="http://www.sellxg.com/index.php">Home</a><span>>></span><a href="http://www.sellxg.com/termsandcondition.php">Terms and Conditions</a></div>
        <div class="mr-1000 mr-1000-location"></div>
      </div>
    </div>
</header>
<div class="row mainaccountdiv">
		<div class="col-md-push-2 col-md-10 col-sm-12 col-xs-12">
			<div class="col-md-9 myaccountinfo">
				<div class="col-sm-12 col-md-12 col-xs-12" style="border:1px solid #E3AE1E;">
					<div class="row" style="padding:9px;background:#E3AE1E;text-align:left;font-size:15px;color:#fff;">
						<font>
							For your Knowledge, we have below provide Our Terms and Conditions 
						</font>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12" style="padding:15px;">
							<div class="col-md-12 col-xs-12 col-sm-12" style="padding:10px 0px;border-bottom:1px solid #eee;">	
								<h4>Terms and Condition</h4>
							</div>
							<div class="col-md-12 col-xs-12 col-sm-12 " style="margin-top:20px;">
							<div class="col-md-12" style="text-align:left;">
							<p style="text-align:justify;">							
								<b class="bigfont"><i class="fa  fa-circle"  aria-hidden="true"></i>General Terms :</b><br>
								<div class="padleft">
									The Terms and Conditions of Website www.sellxg.com adminstrate and guide the User how to use and Behave with Website. The following are notable points in our Website Terms and Conditions<br><BR>
									
								<b><i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>We are Sole Authorised in Modifications -</b>
									We Admin of sellxg.com are only Authorised in Changing or Modifying Content Available and Displayed on the Website.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>The Changes We made on the website will be effective on all Users.<BR>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>We are sole responsible in adding or removing users with or without prior notice.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>In order to stay updated on website info user needs to be Stay on contact with website.<br><br>
								</div>	
								<b class="bigfont"><i class="fa  fa-circle"  aria-hidden="true"></i>User Identification and Password Related Issues :</b><br><br>
								<div class="padleft">	
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>User Id of the Customer is only way in Accessing and Using the Website Services.<BR>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>It is the Duty of Customer to Protect his User Id from Unauthorized Usage. In Case of Duplicated access or Loss of Id Kindly Contact Customer Executive of Sellxg.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Password Safety is also another Important Aspect in User related issues. Any loss of Password Contact Sell Xg Executive immediately.<br><BR>
								</div>	
								<b class="bigfont"><i class="fa  fa-circle"  aria-hidden="true"></i>User Behaviour Related Issues :</b><br><br>
								<div class="padleft">	
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>The use of website must be in compliance with Cyber laws by Govt of India. Any Type of Discreet Approach, Abuse, Disturbing, Defamatory, Provoking, Obscene Should not be performed. Our Approach Towards these will be in Legal Manner.<BR>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>User must not use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Using our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse or other malicious computer software.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Users are not Permitted to use data collected from our website and contact individuals, companies or other persons or entities.<br><BR>
								</div>
								<b class="bigfont"><i class="fa  fa-circle"  aria-hidden="true"></i>Customer (Buyer) Terms and Conditions :</b><br><br>
								<div class="padleft">	
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>For any Buyer related queries the user should always needed to be in contact with sell xg Executive.<BR>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Buyer Should Protect his Account Related to Website Safely.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Any Breaches of these terms and conditions Leads to Following actions by us,<br><br>
									<div class="padleft">
										<i class="fa fa-arrow-right"  aria-hidden="true"></i>We will send you one or more formal warnings<br>
										<i class="fa fa-arrow-right"  aria-hidden="true"></i>We will temporarily suspend your access to our website<br>
										<i class="fa fa-arrow-right"  aria-hidden="true"></i>Permanently Suspend you from accessing our website<br>
										<i class="fa fa-arrow-right"  aria-hidden="true"></i>Commence legal action against you, whether for breach of contract or otherwise<br>
										<i class="fa fa-arrow-right"  aria-hidden="true"></i>Suspend or delete your account on our website.<br><BR>
								</div>
							</div> 
							<b class="bigfont"><i class="fa  fa-circle"  aria-hidden="true"></i>Terms and Conditions of Payment on Sell XG :</b><br><br>
								<div class="padleft">	
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Any Issues with Pay U Money while buying our Product Kindly contact Sell Xg Executive As Soon As Possible.<BR>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Any Issues with Bank Account transaction Kindly contact Sell Xg Executive As Soon As Possible.<br>
									<i class="fa  fa-arrow-circle-o-right"  aria-hidden="true"></i>Any Illegal Approach While Payment of Product either Pay U Money or illegal access to Account Payment force us to Take Legal Action on the user Which may leads to remove the user.<br>
									<br><BR>
								</div>	
							</p>
							</div>
							</div>
							</div>						
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-1">
		</div>
</div>
<!--<div class="lazy-load">

  <div class='Cube panelLoad'>
    <div class='cube-face cube-face-front'>S</div>
    <div class='cube-face cube-face-back'>E</div>
    <div class='cube-face cube-face-left'>L</div>
    <div class='cube-face cube-face-right'>L</div>
    <div class='cube-face cube-face-bottom'>X</div>
    <div class='cube-face cube-face-top'>G</div>
  </div>
</div>-->
<!----footer ----->
<?php
	require_once('footer.php');
?>
</body>

<!--- Vijay add JS all is working Site ---->

<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/scrollReveal.min.js'></script>
<script type='text/javascript' src='js/search.js'></script>
<script type='text/javascript' src='js/js'></script>
<!--<script type='text/javascript' src='http://www.themecop.com/wp/trendz/wp-content/themes/trendz/js/jquery.animateSlider.min.js?ver=4.5.2'></script>-->
<script type='text/javascript' src='js/slider.js'></script>
<script type='text/javascript' src='js/core.min.js'></script>
<script type='text/javascript' src='js/widget.min.js'></script>
<script type='text/javascript' src='js/mouse.min.js'></script>
<script type='text/javascript' src='js/draggable.min.js?'></script>
<script type='text/javascript' src='js/slider.min.js'></script>
<script type='text/javascript' src='js/iris.min.js?'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var trendz_settings = {"latitude":"-37.8173306","longitude":"144.9556518","map_address":"on","map_title":"Envato","loading_time":"40000"};
/* ]]> */
</script>
<script type='text/javascript' src='js/main.js'></script>
</html>

