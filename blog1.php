<?php
	require_once('db.php');
	require_once('head.php');
	if(isset($_REQUEST['subbtn']))
	{
		require_once('usercondition.php');
		$dt=DATE('M d Y h:i A');
		$cmt=mysql_escape_string($_REQUEST['comment']);
		$in=mysql_query("insert into ".BLOG_COMMENT ." values(0,$_REQUEST[bid],'$_SESSION[username]','$cmt','$dt')");
	}
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<body style="background:#eee;">
<?php
	require_once('blogheader.php');
?>
<div class="row blogcontain">
	<div class="col-md-12">
	
	<?php
		$selblog=mysql_query("select * from ".BLOG ." where blog_id=$_REQUEST[bid]");
		$fetblog=mysql_fetch_array($selblog);
	?>

		<div class="col-md-push-2 col-md-8 col-sm-12 col-xs-12 blogpost" style="margin-top:30px;">	
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:right;">
				<i class="fa fa-image" style="color:#ccc;font-size:20px;"></i>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
				<font style="font-weight:bold;color:#777;"><?php echo $fetblog[6]; ?></font>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
				<h2 style="color: #4c4c4c;transition: all .3s;font-size: 32px;margin: 15px 0 20px;padding: 0;font-weight: 700;font-family: 'Lora',serif;font-style: italic;text-transform: initial;">
				<?php echo $fetblog[1]; ?>
				</h2>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
				<i>Posted By  </i><b><?php echo $fetblog[2]; ?></b>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
				<a href="showblog.php?bid=<?php echo $fetblog[0];?>" style="font-weight:bold;color:#888;text-transform:uppercase;">
					<?php 
						$selcom=mysql_query("select count(*) from ".BLOG_COMMENT ." where blog_id=$_REQUEST[bid]");
						$fetcom=mysql_fetch_array($selcom);	
						echo $fetcom[0]." comments"
					?>
				</a>
			</div>
			<div class="col-md-12">
				
				<!--<img src="images/<?php echo $fetblog[4] ?>" class="img img-responsive"  width="100%" style="margin-top:20px;"/>
			</div>--->
			<div class="col-md-12 col-sm-12 col-xs-12"><br>
				<p style="padding: 0 40px;font-family: Open Sans,sans-serif;font-size: 16px;font-weight: 400;line-height: 1.625;letter-spacing: .01rem;color: #666;text-align: justify;">
					 
						<u><b>INTRODUCTION :</b></u><br><br>
						As The Discussion on GST is going all Around let us Discuss it's Impact on Textile Industry and How Dealing with Overstock Products actually Beneficial by Application of GST.<br><br>
						<u><b>GST BILL (Goods and Services Tax Bill) :</b></u><br><br>
						
							The Goods and Services Tax Bill or GST Bill officially known as The Constitution (One Hundred and Twenty-Second Amendment) Bill, 2014, proposes a national Value Added Tax to be implemented in India from 1 April 2017.
							<br><br>
							
							Goods and Services Tax would be levied and collected at each stage of sale or purchase of goods or services based on the input tax credit method.
							<br><br>
							This method allow GST-registered companies to claim tax credit to the value of GST they paid on purchase of goods or services as of their commercial activity.
							<br><br>
							<u><b>BRIEF LOOK ON CURRENT TRENDS IN TEXTILES :</b></u><br><br>
							<ul >
							<li style="text-decoration:none important;"><b>CLASSIFICATION OF FABRICS - </b>
							Fabrics vs Garments in which for example Sarees should be valued as fabrics or Ready made garments.
							</li>
							<li><b>Differences in Tax Rates - </b>
							Effective taxation for composite mills higher compared that of power looms.
							</li>
							<li><b>FABRIC PRODUCTION TREND - </b>
							The production of fabric in India is almost stagnant from 2009-10 to 2013-14, with a CAGR of 1%. Cotton fabric and blended fabric has grown at 5% and 7% per annum, while 100% non-cotton fabric has shown de-growth of 7% in the same period.
							</li>
							<li><b>MARKET SIZE - </b>
							The Indian textiles industry, currently estimated at around US$ 108 billion, expected to reach US$ 223 billion by 2021.
							</li>
							<li><b>Bias mentality in Fabrics - </b>
							Cotton vs Manmade fibre as Cotton received better compared to manmade Fibre.
							</li>
							</ul><br><br>
							<img src="images/Value.jpg" class="img img-responsive"  width="100%" style="margin-top:20px;"/>
							
							<br><br>
							<center><u><b>INDIAN TEXTILES AND APPAREL MARKET SIZE :</b></u></center><br><br>
							<u><b>Impact and Key Effects of GST :</b></u><br><br>
							<ul>
							<li>
							Increase in tax burden from 9.3% to possibly 12% which may lead to a reduction in demand
							</li>
							<li>
							Greater efficiency in production which leads to downward movement in prices
							</li>
							<li>
							Exports may go up due to true zero rating
							</li>
							<li>
							<b>LOWER TAX RATES</b> will follow from GST covering all goods and services, with tax only on value addition and set-offs against taxes on inputs/previous purchases
							</li>
							<li>
							There are number of disputes in relation to the current taxation. The fabrics versus garment classification, differential taxation for cotton and manmade Fibre and composite mills taxed at a higher rate than the power looms are plaguing the industry. The GST will be a uniform rate of tax for such disputed items and likely to well-disposed settlement for the disputes.
							</li>
							<li>
							The Below Picture Describes an Example of applying GST in Textile Industry
							</li>
							</ul>
							<img src="images/GStt.jpg" class="img img-responsive"  width="100%" style="margin-top:20px;"/>
							<center><u><b>AN EXAMPLE ABOUT APPLICATION OF GST</b></u></center>
							<u><b>SURPLUS/OVERSTOCK GOOD :</b></u>
							A <b>surplus<b> is the amount of a good or resource that exceeds the portion that needs utilized or Sold.<br><br>
							Surplus stock is disposed of to convert the unwanted items of stock in money. Further supplies of such items of stock must be stopped or postponed.<br><br>
							<u><b>Disposal of surplus can be done by following methods</b></u>
							<ul>
							<li>a)	Use within the firm</li>
							<li>b)	Return to the supplier</li>
							<li>c)	Direct sale to another firm</li>
							<li>d)	Sale to a dealer or broker</li>
							<li>e)	Sale to employer</li>
							<li>f)	Donations to charitable and educational institutions.</li>
							<li>g)	Some combination of the preceding methods.</li>
							<li>h)	Sale to dealer middlemen.</li>
							</ul>
							However These Methods incur Losses for Seller and Government.<br><br>
							The Seller tends to Lose the Marginal amount of Money that he expect to make by selling Surplus goods. And The Government Loses the Taxes  that can be applied on them before becoming a Surplus.<br><br>
							<u><b>CATEGORIES OF OVERSTOCK/SURPLUS GOODS :</b></u><br><br>
							There are 9 Types of Categories in Over Stock/Surplus. They are as follows,
							<ul>
							<li>1.	Khadi and handlooms </li>
							<li>2.	Cotton textiles</li>
							<li>3.	Woollen textiles </li>
							<li>4.	Silk textiles	</li>
							<li>5.	Artificial silk and synthetic fibre textiles </li>
							<li>6.	Jute, hemp, and mesta textiles </li>
							<li>7.	Carpet weaving </li>
							<li>8.	Ready-made garments </li>
							<li>9.	Miscellaneous textile products </li>
							</ul>
							<u><b>HOW SELL XG BENEFITS FROM GST :</b></u><br><br>
							Sellxg is Customer Supportive Online Portal which follows Authentic Way to Buy & Sell Surplus Textile Goods for Best Price With Global Reach.<br><br>
							An End to End to Service is provided by sellxg through open window policy. <br><br>
							<u><b>BENEFITING FROM GST</b></u>
							<ul>
							<li>i.	Overstock/Surplus Goods provide a big Dent to the Textile Industry.</li>
							<li>ii.	Their Existence always Burdens the Taxation System as they sold out in illegal and unsystematic Approach.</li>
							<li>iii.	SellXg follows Systematic and Legal Approach while Dealing with Surplus Goods.</li>
							<li>iv.	We Process Thematic and Rightful Transaction between Buyer and Seller of Surplus products</li>
							<li>v.	By Following Concept of Digital India we created Mobile Application and Web Portal that provides every help required in selling of Surplus goods.</li>
							<li>vi.	We started as a team of 4 members now expanded to more than 25 members and bound to increase upto 50 by end of Financial year 2016-2017.</li>
							</ul>
							The sale of Lot Product/Surplus product is done in two ways. They are :<br><br>
							<b>1.	DIRECT SALE -</b>Buyer can Directly Purchase the Product<br><br>
							<b>2.	AUCTION -</b>Buyers can Participate in Auction to Purchase the Product.
							<br><br>
							<img src="images/frontb.png" class="img img-responsive"  width="100%" style="margin-top:20px;"/>
							
							<br><br>
							<center><u><b>WORKING PATTERN OF SELL XG</b></u></center><br><br>
							<u><b>TAXATION OF GOOD IN TEXTILE INDUSTRY :</b></u><br></br>
							<b>I.	CENVAT -</b>Central Value Added Tax<br>
							<b>II.	Countervailing duty </b><br>
							<b>III.	VAT -</b>value-added tax<br>
							<b>IV.	BASIC DUTY</b>
							<img src="images/taxees.jpg" class="img img-responsive"  width="100%" style="margin-top:20px;"/>
							
							<br><br>
							<center><u><b>MAJOR TAXES ON GOODS</b></u></center>
							<b>MODUS OPERANDI OF SELL XG ON OVERSTOCK:</b>
							<br><br>
							We Follow open door policy while Dealing with Surplus goods. The Following points represent our Modus Operandi in dealing the Surplus Goods.
							<br><br>
							I.	 Targeting Over Stock goods which are well Carved in Textile Sector.<br><br>
							II.	Enabling Secured and Legal Transactions of Payment by Escrow Account.<br><br>
							III.	Dealing with Surplus Goods according to current Taxation Policy and bringing the whole operation under the Surface Area of GST.<br><br>
							IV.	To make it more reliable we have Transparent return policy.<br><br>
							
							
				</p>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<hr style="padding:0.1px;background:#eee;"/>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-12" style="text-align:center">
				<h4 style="font-weight:bold;">Give Your Comment On This Blog</h4><br>
			</div>
			<form name="blogcomment" method="post">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12 col-sm-12 col-xs-12 logintext">
					<div class="col-md-12" style="text-align:left;margin-lefT:1px;">
						<lable style="font-size:13px;">Comment<font style="color:red;font-size:16px;">*</font></lable>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<textarea class="form-control" required name="comment" required rows="4" style="resize:none;	"></textarea>
					</div>
				</div>	
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12" style="margin:10px 20px;">	
				<div class="col-md-12 col-sm-12 col-xs-12">			
					<button type="submit" name="subbtn" style="color:white;font-size:15px;background:#F86E7D;border:none;padding:10px;letter-spacing:1px;width:150px;"><i class="fa fa-paper-plane" style="font-size:15px;"></i>  Post</button>				
				</div>
			</div>
			</form>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<hr style="padding:0.1px;background:#eee;"/>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-12" style="text-align:center">
				<h4 style="font-weight:bold;">Total Comment Of This Blog</h4><br>
			</div>
			<div class="col-sm-12 col-xs-12 col-md-12">
				<?php 
					$selcom=mysql_query("select count(*) from ".BLOG_COMMENT ." where blog_id=$_REQUEST[bid]");
					$fetblog=mysql_fetch_array($selcom);
					if($fetblog[0]==0)
					{
						echo "No Comment Found";
					}
					else
					{
							$i=0;
						$selcom=mysql_query("select * from ".BLOG_COMMENT ." where blog_id=$_REQUEST[bid]");
						while($fetblog=mysql_fetch_array($selcom))
						{
								$i++;
				?> 	
						<div class="col-md-12 col-sm-12 col-xs-12">
								<p style="font-size:13px;"><font style="font-size:13px;padding:8px 11px;border-radius:100%;background:#F86E7D;color:#fff;margin-right:10px;"> <?php echo $i; ?></font><b style="font-size:15px;text-transform:capitalize;"><?php echo $fetblog[2]; ?></b> :  <p style="text-align:justify;"><?php echo $fetblog[3]; ?></p?</p>
						</div>
				<?php
						}
					}
				?>
			</div>
		</div>
	</div>
</div> 
<?php
	require_once('blogfooter.php');
?>
</body>

</html>
