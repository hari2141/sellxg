<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Description" content="Sellxg is a platform where Manufacturer, Wholeseller, Retailer can buy and sell surplus fabric, home textile, salwar suit,saree at best price of whole textile industry.">
<meta name="keywords" content="Overstock Textiles, Surplus Fabric, Textile Stocks, Extrastock, Yarn Lots, Home Textile">
<meta name="og_url" property="og:url" content="http://www.sellxg.com"></meta>
<meta name="author" content="SellXG">
<link rel="shortcut icon" href="inc/image/title_logo.png">
<link rel="canonical" href="http://www.sellxg.com/">
<!-------------------------------------CSS Start------------------------------------------->
<link href="http://www.sellxg.com/inc/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="inc/css/style.css" rel="stylesheet" type="text/css" />
<link href="inc/css/home1.css" rel="stylesheet" type="text/css" />
<link href="inc/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-------------------------------------CSS End------------------------------------------->

<!-------------------------------------JS Start------------------------------------------->
<script src="inc/js/jquery.min.js"></script>
<script src="inc/js/bootstrap.min.js"></script>
<script>
	$(window).scroll(function(){
	  var head = $('.header'),
		  scroll = $(window).scrollTop();
	
	  if (scroll >= 100) head.addClass('fixed');
	  else head.removeClass('fixed');
	});
</script>
<!-------------------------------------JS End------------------------------------------->

<title></title>
</head>

<body>
<header class="header">
    <div class="side-slider-div">
        <div class="row nav-row">
            <div class="container">
                <div class="col-xs-12 col-sm-6 social-bar">
                    <span><a href="http://www.facebook.com/sellextragoods"><i class="fa fa-facebook" aria-hidden="true"></i></a></span>
                    <span><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></span>
                    <!--<span><a href=""><img src="http://www.sellxg.com/inc/image/nav-googlepl.png" height="17" /></a></span>-->
                    <span><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></span>
                    <span><a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                    <span><a href=""><i class="fa fa-pinterest" aria-hidden="true"></i></a></span>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="nav-right">
                        <!--<div class="drop-nav">currency<span><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>-->
                        <div class="drop-nav">
                            <select>
                                <option>currency</option>
                                <option>rupees</option>
                                <option>dollar</option>
                                <option>pound</option>
                            </select>
                        </div>
                        <!--<div class="drop-nav">language<span><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>-->
                        <div class="drop-nav">
                            <select>
                                <option>language</option>
                                <option>english</option>
                                <option>gujarati</option>
                                <option>hindi</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row center-head no-margin">
            <div class="container">
                <div class="col-md-3"></div>
                <div class="col-md-9 main-head">
                    <!--<div class="head">
                        <div class="header_head_div">Provide Email ID for more Information</div>
                        <div class="email_div">
                            <div class="email_box"></div>
                        </div>
                    </div>-->
                    <div class="nav-content">
                        <div class="menu"><a href=""><i class="fa fa-user" aria-hidden="true"></i>my account</a></div>
                        <div class="menu"><a href=""><i class="fa fa-heart" aria-hidden="true"></i>my wishlist</a></div>
                        <div class="menu"><a href=""><i class="fa fa-credit-card" aria-hidden="true"></i>checkout</a></div>
                        <div class="menu"><a href=""><i class="fa fa-lock" aria-hidden="true"></i>login / register</a></div>
                        <div class="menu"><a href=""><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span><span>shopping cart (0)</span><!--<p>0 Item<i class="fa fa-angle-down" aria-hidden="true"></i></p>--></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid bottom-header">
            <div class="container height100">
                <div class="col-sm-3"></div>
                <div class="col-sm-9 height100">
                    <div class="head-content">
                        <ul class="main-menu">
                            <li><a href="">All Electronics</a></li>
                            <li><a href="">Fashion</a></li>
                            <li><a href="">Home & Living</a></li>
                            <li><a href="">Accessories </a></li>
                            <li><a href="">Health & Beauty</a></li>
                            <li><a href="">Jewellery</a></li>
                            <li><a href="">Deals</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="logo-main-div">
            <div class="logo-content-div">
                <img src="http://creadigol.biz/sellxg/inc/image/comingsoon_page_logo.png" class="img_responsive">
            </div>
            <div class="side_line"></div>
        </div>
        <!--<div class="side-bar-div">
        	<div class="slider-tab-div">
            	<div class="tab"></div>
                <div class="tab active"></div>
                <div class="tab"></div>
                <div class="tab"></div>
            </div>
        </div>-->
        <div id="myCarousel" class="carousel side-bar-div" data-ride="carousel">
            <ol class="carousel-indicators slider-tab-div">
                <li data-target="#myCarousel" data-slide-to="0" class="tab active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class="tab"></li>
                <li data-target="#myCarousel" data-slide-to="2" class="tab"></li>
                <li data-target="#myCarousel" data-slide-to="3" class="tab"></li>
            </ol>
        </div>
                    
        <div class="slider-div"></div>
        
        <div class="main-container"><!--container-->
        	<div class="col-xs-11 no-padding slider">
                <!--<img src="http://www.sellxg.com/inc/image/home-slider1.png" class="main-img" />
                <div class="slider_content">
                    <div class="height_center_div">
                        <div class="slider_head">Discounts Everywhere</div>
                        <img src="http://www.sellxg.com/inc/image/slider_head_border.png" />
                        <div class="slider_text">Get 20% Discount on every purchase over 200$ Sign up to get our free membership discount card. So why are you waiting for ?</div>
                        <div class="slider_detail_btn_bar">
                            <div class="slider_detail_btn">VIEW ALL ITEMS</div>
                        </div>
                    </div>
                </div>-->
                <div id="myCarousel" class="carousel slide height100" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators slider-tab-div">
                      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                      <li data-target="#myCarousel" data-slide-to="1"></li>
                      <li data-target="#myCarousel" data-slide-to="2"></li>
                      <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner height100" role="listbox">
                
                      <div class="item height100 active">
                        <img src="http://www.sellxg.com/inc/image/home-slider1.png" alt="Chania" >
                        <div class="carousel-caption">
                          <h3>Chania</h3>
                          <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                      </div>
                
                      <div class="item height100">
                        <img src="http://www.sellxg.com/inc/image/home-slider1.png" alt="Chania" >
                        <div class="carousel-caption">
                          <h3>Chania</h3>
                          <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                        </div>
                      </div>
                    
                      <div class="item height100">
                        <img src="http://www.sellxg.com/inc/image/home-slider1.png" alt="Flower" >
                        <div class="carousel-caption">
                          <h3>Flowers</h3>
                          <p>Beatiful flowers in Kolymbari, Crete.</p>
                        </div>
                      </div>
                
                      <div class="item height100">
                        <img src="http://www.sellxg.com/inc/image/home-slider1.png" alt="Flower" >
                        <div class="carousel-caption">
                          <h3>Flowers</h3>
                          <p>Beatiful flowers in Kolymbari, Crete.</p>
                        </div>
                      </div>
                  
                    </div>
                
                    <!-- Left and right controls -->
                    <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>-->
                  </div>
            </div>
        </div>
        <div class="foot"></div>
    </div>
</header>
<div class="clearfix"></div>
<div class="section-1">
	<div class="section-1-container">
    	<div class="search-bar-div">
        	<input type="search" class="col-xs-11 col" placeholder="Search Everything At Here...">
            <div class="col-xs-1 col">
            	<input type="submit" value="" />
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-2">
	<div class="container">
        <div class="row">
            <div class="col-sm-10">FEATURED PRODUCTS</div>
            <div class="col-sm-2 sub-head">VIEW ALL <i class="fa fa-angle-right" aria-hidden="true"></i></div>
        </div>
        <div class="row">
        	<div class="col-xs-12 col-md-4">
            	<div class="box-col">
                	<img src="inc/image/home-sofa.png" class="img-responsive" />
                    <div class="lable-div">
                    	<div class="detail-div">
                        	<div class="detail-content">
                            	<div class="head-div">Outdoor Sofa in Chocolate</div>
                                <div class="detail">For 2 Person<br />Lorem Ipsum Dolor it</div>
                            </div>
                        </div>
                        <div class="price-div">
                        	<div class="price-content">
                            	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />
                                $ 59
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
            	<div class="box-col">
                	<img src="inc/image/home-cap.png" class="img-responsive" />
                    <div class="lable-div">
                    	<div class="detail-div">
                        	<div class="detail-content">
                            	<div class="head-div">Fashionable Women’s Hat</div>
                                <div class="detail">Leather Material<br />Get 20% Discount on Hat</div>
                            </div>
                        </div>
                        <div class="price-div">
                        	<div class="price-content">
                            	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />
                                $ 59
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
            	<div class="box-col">
                	<img src="inc/image/home-hairband.png" class="img-responsive" />
                    <div class="lable-div">
                    	<div class="detail-div">
                        	<div class="detail-content">
                            	<div class="head-div">Silver Crown for Bribe</div>
                                <div class="detail">Diamond<br />2 Year Warrenty of Diamond</div>
                            </div>
                        </div>
                        <div class="price-div">
                        	<div class="price-content">
                            	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />
                                $ 59
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-3">
	<div class="container">
    	<div class="main-head">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="main">BEST DEAL</div>
                <div class="line"></div>
            </div>
        </div>
        <h4>LATEST PRODUCTS</h4>
        <div class="head-border-div">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="dot"></div>
                <div class="line"></div>
            </div>
        </div>
        <div class="product_div">
        	<div class="row">
            	<div class="col-xs-12 col-sm-6 col-md-3">
                	<div class="col">
                    	<div class="img_div">
                    		<img src="inc/image/home-goldring.png" class="img_responsive" />
                        </div>
                        <div class="lable_div">
                        	<div class="lable_box">
                            	<div class="lable">golden rings</div>
                            </div>
                        </div>
                        <p>Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus ut…</p>
                        <div class="p_border_div">
                        	<div class="width_center pos_rel">
                            	<div class="border"></div>
                            </div>
                        </div>
                        <div class="price_div">$79</div>
                        <div class="foot">
                        	<div class="cont_div">
                            	<div class="cont">
                                	<span><i class="fa fa-heart" aria-hidden="true"></i></span>
                                    <span>17</span>
                                </div>
                                <div class="cont">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </div>
                                <div class="cont">
                                	<i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                	<div class="col">
                    	<div class="img_div">
                    		<img src="inc/image/home-spree.png" class="img_responsive" />
                        </div>
                        <div class="lable_div">
                        	<div class="lable_box">
                            	<div class="lable">vera wang spree</div>
                            </div>
                        </div>
                        <p>Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus ut…</p>
                        <div class="p_border_div">
                        	<div class="width_center pos_rel">
                            	<div class="border"></div>
                            </div>
                        </div>
                        <div class="price_div">$79</div>
                        <div class="foot">
                        	<div class="cont_div">
                            	<div class="cont">
                                	<span><i class="fa fa-heart" aria-hidden="true"></i></span>
                                    <span>17</span>
                                </div>
                                <div class="cont">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </div>
                                <div class="cont">
                                	<i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                	<div class="col">
                    	<div class="img_div">
                    		<img src="inc/image/home-watch.png" class="img_responsive" />
                        </div>
                        <div class="lable_div">
                        	<div class="lable_box">
                            	<div class="lable">armani watches</div>
                            </div>
                        </div>
                        <p>Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus ut…</p>
                        <div class="p_border_div">
                        	<div class="width_center pos_rel">
                            	<div class="border"></div>
                            </div>
                        </div>
                        <div class="price_div">$79</div>
                        <div class="foot">
                        	<div class="cont_div">
                            	<div class="cont">
                                	<span><i class="fa fa-heart" aria-hidden="true"></i></span>
                                    <span>17</span>
                                </div>
                                <div class="cont">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </div>
                                <div class="cont">
                                	<i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                	<div class="col">
                    	<div class="img_div">
                    		<img src="inc/image/home-shoes.png" class="img_responsive" />
                        </div>
                        <div class="lable_div">
                        	<div class="lable_box">
                            	<div class="lable">bribes shoes</div>
                            </div>
                        </div>
                        <p>Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus ut…</p>
                        <div class="p_border_div">
                        	<div class="width_center pos_rel">
                            	<div class="border"></div>
                            </div>
                        </div>
                        <div class="price_div">$79</div>
                        <div class="foot">
                        	<div class="cont_div">
                            	<div class="cont">
                                	<span><i class="fa fa-heart" aria-hidden="true"></i></span>
                                    <span>17</span>
                                </div>
                                <div class="cont">
                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </div>
                                <div class="cont">
                                	<i class="fa fa-share" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-4">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 col">
            	<div class="col-box">
                	<div class="content-div">
                    	<div class="img_div">
                        	<img src="inc/image/home-shipping.png" class="img-responsive" />
                        </div>
                        <div class="cont">
                        	<div class="main-cont">
                            	<h4>FREE SHIPPING</h4>
                                <p><i>free shipping every $100</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col">
            	<div class="col-box">
                	<div class="content-div">
                    	<div class="img_div">
                        	<img src="inc/image/home-moneyback.png" class="img-responsive" />
                        </div>
                        <div class="cont">
                        	<div class="main-cont">
                            	<h4>MONEY BACK</h4>
                                <p><i>send to you within 24 hours</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col">
            	<div class="col-box">
                	<div class="content-div">
                    	<div class="img_div">
                        	<img src="inc/image/home-24hours.png" class="img-responsive" />
                        </div>
                        <div class="cont">
                        	<div class="main-cont">
                            	<h4>24 HOURS SUPPORT</h4>
                                <p><i>call : 10000 2456 234</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-5">
	<div class="container">
    	<div class="main-head">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="main">GREAT DEAL</div>
                <div class="line"></div>
            </div>
        </div>
        <h4>SPECIAL PRODUCTS</h4>
        <div class="head-border-div">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="dot"></div>
                <div class="line"></div>
            </div>
        </div>
        <div class="product_div">
        	<div class="row">
            	<div class="col-xs-12 col-md-4">
                	<div class="col">
                    	<div class="head-div">
                        	<div class="cart-div">
                            	<div class="content-div">
                                	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />$ 80:00
                                </div>
                            </div>
                            <div class="title-div">algarva indoor bad</div>
                        </div>
                    	<img src="inc/image/home-cross-border.png" class="img-responsive" />
                        <div class="img-div">
                        	<img src="inc/image/home-bad.png" class="img-responsive" />
                        </div>
                        <div class="lable-div">
                        	<div class="lable">
                            	<div class="lable-content">Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus uttar</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                	<div class="col">
                    	<div class="head-div">
                        	<div class="cart-div">
                            	<div class="content-div">
                                	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />$ 80:00
                                </div>
                            </div>
                            <div class="title-div">diamond rings</div>
                        </div>
                    	<img src="inc/image/home-cross-border.png" class="img-responsive" />
                        <div class="img-div">
                        	<img src="inc/image/home-diamondring.png" class="img-responsive" />
                        </div>
                        <div class="lable-div">
                        	<div class="lable">
                            	<div class="lable-content">Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus uttar</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                	<div class="col">
                    	<div class="head-div">
                        	<div class="cart-div">
                            	<div class="content-div">
                                	<i class="fa fa-shopping-cart" aria-hidden="true"></i><br />$ 80:00
                                </div>
                            </div>
                            <div class="title-div">personal care poducts</div>
                        </div>
                    	<img src="inc/image/home-cross-border.png" class="img-responsive" />
                        <div class="img-div">
                        	<img src="inc/image/home-nivea.png" class="img-responsive" />
                        </div>
                        <div class="lable-div">
                        	<div class="lable">
                            	<div class="lable-content">Pellentesque in rhoncus? In nec cursus et, magna odio? Enim, magnis eu, purus cursus uttar</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-6">
	<div class="container">
    	<div class="main-head">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="main">BEST BLOG</div>
                <div class="line"></div>
            </div>
        </div>
        <h4>LATEST BLOG</h4>
        <div class="head-border-div">
        	<div class="width_center pos_rel">
            	<div class="line"></div>
                <div class="dot"></div>
                <div class="line"></div>
            </div>
        </div>
        <p class="main">Nullam in felis malesuada lacus tempor faucibus. Sed massa dui, auctor sed venenatis vel, suscipit et orci. Aliquam suscipit laoreet ultrices. Integer odio tortor, vestibulum.</p>
        <div class="main-div">
        	<div class="row">
            	<div class="col-xs-12 col-sm-7 main-img-div">
                	<!--<img src="inc/image/home-fabrics.png" class="img-responsive" />-->
                    <div class="img-inborder-div">
                    	<div class="img-description-div">
                        	<div class="head-div">the best brand of the world</div>
                            <center><img src="inc/image/home-img-border.png" /></center>
                            <div class="main-detail-div">
                            	<div>
                                	<div class="head-div">Posted By</div>
                                    <div class="colon-div">:</div>
                                    <div class="head-detail-div">Creadigol</div>
                                </div>
                                <div>
                                	<div class="head-div">Posted on</div>
                                    <div class="colon-div">:</div>
                                    <div class="head-detail-div">1/4/2016</div>
                                </div>
                            </div>
                            <p class="img-descriptionp">Nullam in felis malesuada lacus tempor faucibus. Sed massa dui, auctor sed venenatis vel, suscipit et orci. Aliquam suscipit laoreet ultrices. Integer odio tortor, vestibulum.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="section-7">
	<div class="header-div">
    	<div class="main-header-div">
        	<div class="main-header">
                <div class="row">
                    <div class="col-xs-12 col-md-3 head-div">CLIENTS TESTIMONIALS</div>
                    <div class="col-xs-12 col-md-7 photo-div">
                    	<ul>
                        	<li>
                            	<img src="inc/image/home-client1.png" class="img-circle img-responsive" />
                                <!--<center><img src="inc/image/home-downarrow.png" /></center>-->
                                <!--<div class="downarrow"></div>-->
                            </li>
                            <li><img src="inc/image/home-client2.png" class="img-circle img-responsive" /></li>
                            <li><img src="inc/image/home-client3.png" class="img-circle img-responsive" /></li>
                            <li><img src="inc/image/home-client4.png" class="img-circle img-responsive" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="center-div">
    	<div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <div class="img-div"><img src="inc/image/home-client2.png" class="img-circle img-responsive" /></div>
                </div>
                <div class="col-xs-12 col-md-9">
                    <p>Nullam in felis malesuada lacus tempor faucibus. Sed massa dui, auctor sed venenatis vel, suscipit et orci. Aliquam suscipit laoreet ultrices. Integer odio tortor, vestibulum.auctor sed venenatis vel, suscipit et orci. Aliquam suscipit laoreet ultrices. Integer odio tortor, vestibulum.auctor sed venenatis vel, suscipit et orci. Aliquam suscipit laoreet ultrices.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="border-div">
    	<!--<img src="inc/image/home-clientborder.png" />-->
        <div class="container height100">
        	<div class="row height100">
            	<div class="col-xs-9">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 name-div">
                        	<span>Miss. Aliya</span>
                            <span><i>Seniore Manager</i></span>
                        </div>
                        <div class="col-xs-12 col-sm-5 star-div">
                        	<div class="stars">
                            	<img src="inc/image/home-fillstar.png" />
                                <img src="inc/image/home-fillstar.png" />
                                <img src="inc/image/home-fillstar.png" />
                                <img src="inc/image/home-blankstar.png" />
                                <img src="inc/image/home-blankstar.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<footer>
    <div class="top-div">
        <div class="container">
            <div class="main-div">
            	<div class="row main-div-content">
                	<div class="col-xs-12 col-sm-9 sbscrib-div">
                    	<div class="row">
                        	<div class="col-xs-12 col-md-4 title">SIGN UP FOR NEWSLETTER</div>
                            <div class="col-xs-12 col-md-8">
                                <div class="input-div">
                                    <input type="email" placeholder="Enter your email id..." />    
                                    <input type="submit" value="SUBSCRIBE" />                      
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 social-icon-div">
                    	<a href="http://www.facebook.com/sellextragoods"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                        <i class="fa fa-youtube" aria-hidden="true"></i>
                        <i class="fa fa-pinterest" aria-hidden="true"></i>
                        <i class="fa fa-dribbble" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer-div">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12 col-md-3 contact-div">
                	<div class="head-div">contact us</div>
                    <div class="address-div">
                    	<div class="head-div">Warehouse Offices</div>
                        <div class="address">Lorem Ipsum is simply dummy text of the printing and typesettin ,000001 , UK<br />Phone :91 123-4567-890
<br />Email: info@goldenelegance.co.uk</div>
                    </div>
                    <div class="address-div">
                    	<div class="head-div">retail store</div>
                        <div class="address">54321 Street name, California, USA<br />Phone :91 123-4567-890</div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 foot-detail-div">
                	<div class="row">
                    	<div class="col-xs-12 col-md-3">
                        	<div class="head-div">information</div>
                            <div class="detail-div">
                            	<div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>About Us</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Faqs</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Affiliates</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Contact</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                        	<div class="head-div">quick links</div>
                            <div class="detail-div">
                            	<div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>All Electronics</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Fashion</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Home & Living</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Accessories</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Health & Beauty</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Jewellery</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                        	<div class="head-div">my account</div>
                            <div class="detail-div">
                            	<div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>my orders</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>my credit slips</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>my addresses</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>my personal info</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>my vouchers</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                        	<div class="head-div">extra</div>
                            <div class="detail-div">
                            	<div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>terms & condition</span>
                                </div>
                                <!--<div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>condition</span>
                                </div>-->
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>privacy policy</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>easyreturn</span>
                                </div>
                                <div class="detail">
                                	<span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    <span>Suport</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-div">
    	<div class="container">
        	<div class="row">
            	<div class="col-xs-12 col-md-6 copyright-div">Copyright 2015 sellxg.com All Rights Reserved</div>
                <div class="col-xs-12 col-md-6 pay-icon-div">
                	<div class="icon-div">
                    	<span><img src="inc/image/home-mastercard.png" /></span>
                        <span><img src="inc/image/home-visacard.png" /></span>
                        <span><img src="inc/image/home-paypal.png" /></span>
                        <span><img src="inc/image/home-maestro.png" /></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script>
	$('.carousel').carousel()
</script>
</body>
</html>